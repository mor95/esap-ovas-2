const presentation = require('presentation')
const ovaAnimateElements = require('ova-animate-elements')
const animatePerSlide = require('animate-per-slide')

module.exports = {
    props: {},
    animateSlides: function(){
        var self = this
        $('.slide').each(function(){
            ovaAnimateElements($(this).children())
        })

        animatePerSlide.setAnimationClasses({
            $slide: self.$slide,
            action: 'add'
        })
    },
    addCharacterSelectionEvents: function(){
        var self = this
        self.$slide.find('.character-selector').on('click', function(){
            var id = $(this).attr('data-id')
            self.props.selectedId = id
            $('html').attr('data-selected-character-id', id)
            $(this).addClass('selected')
                .siblings().removeClass('selected')
            presentation.switchToSlide({
                slide: 'next'
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.addCharacterSelectionEvents()
        self.animateSlides()
        $('.navigation-buttons .home').off('click').on('click', function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })
        })
    }
}