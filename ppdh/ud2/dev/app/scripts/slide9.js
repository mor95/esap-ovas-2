module.exports = {
    props: {
        texts: {}
    },
    addTextareaEvents: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.addTextareaEvents()
    }
}