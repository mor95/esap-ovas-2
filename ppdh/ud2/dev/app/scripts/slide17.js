const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup3 = require('conceptsGroup3.json')
const presentation = require('presentation')


module.exports = {
    addEventListeners: function(){

    },
    onSlide: function(){
        this.finish()
        return true
    },
    finish: function(){
        var self = module.exports
        self.finished = true
        var targetConcepts = _.pick(concepts, [
            'Habeas data',
        ])

        return self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup3))
        .then(function (t) {

            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Acción de cumplimiento': true
                },
                action: 'add'
            })
        })

    }
}