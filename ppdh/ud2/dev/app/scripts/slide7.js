const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')

module.exports = {
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/videoModal.hbs')({
                videoLink: _.get(self, 'media.AV.mediaid')
            }),
            customClass: 'ova-themed bigger video'
        }))
    },
    addEventListeners: function(){}
}