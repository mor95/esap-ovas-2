<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Mecanismos Nacionales de protección de derechos humanos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
        {include file='styles2.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Mecanismos Nacionales de protección de derechos humanos</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad 2</h2>

        <h2>Ahora, vamos a poner en practica todos los conceptos, complete los espacios con sus ideas y planteamientos</h2>

        <h2 class="question">¿Qué noticia llamó más su atención?</h2>
        <h2 class="answer">- {$a[0][0]}</h2>

        <h2 class="question">¿Por qué su selección?</h2>
        <h2 class="answer">- {$a[0][1]}</h2>

        <h2 class="question">¿Qué derecho o derechos se vulneraron en la noticia seleccionada?</h2>
        <h2 class="answer">- {$a[1][0]}</h2>

        <h2 class="question">Según sus conocimientos y haciendo uso de los conceptos que hasta ahora ha adquirido, ¿Cuál mecanismo de protección considera pertinente aplicar en este caso?</h2>
        <h2 class="answer">- {$a[2][0]}</h2>

        <h2 class="question">Ahora, argumente su decisión al respecto</h2>
        <h2 class="answer">- {$a[3][0]}</h2>

        <h2 class="question">¿Qué ley rige el proceso que seleccionó?</h2>
        <h2 class="answer">- {$a[4][0]}</h2>

        <h2 class="question">¿Qué personas se vinculan al proceso?</h2>
        <h2 class="answer">- {$a[4][1]}</h2>

        <hr>
        <h2 class="note">Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
