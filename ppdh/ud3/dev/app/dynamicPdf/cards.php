<?php
    $cards = Array(
        "Grupo etnico",
        "Personas que comparten condiciones de lenguaje, alimentación y cultura",
        "Sujetos de especial protección, que poseen un mayor número de derechos ",
        "Niños, niñas y adolescentes",
        "Se reconoció sus ciudadanía después de 1930",
        "Mujer",
        "Población vulnerable",
        "Condiciones que hacen que alguien esté en riesgo.",
        "Tienen dificultades para ejercer sus derechos por sus limitaciones",
        "Personas con discapacidad",
        "LGBTI",
        "Personas con preferencias sexuales diversas",
        "Inclusión",
        "Política que busca la equidad entrelas personas",
        "Raizal",
        "Euro-afro-descendiente",
        "Género",
        "Construcción social de los roles de las personas. ",
        "Adulto mayor",
        "Con edades entre 60 y 69 años.",
        "Proceso de desarrollo de la vida de los seres humanos",
        "Ciclo de vida"
    );
?>