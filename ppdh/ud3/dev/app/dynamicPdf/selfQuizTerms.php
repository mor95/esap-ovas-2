<?php
    $selfQuizTerms = Array(
        "Políticas públicas",
        "Acciones afirmativas",
        "Derechos humanos",
        "Mecanismos de protección",
        "Ciclo de vida – pertenencia a grupos étnicos",
        "Plan de incidencia",
        "Mapa de actores",
        "Problemas públicos",
        "Justiciabilidad de los derechos humanos",
        "Disponibilidad"
    )
?>