const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')
const hexagonTexts = require('hexagonTexts.json')
const concepts = require('concepts.json')
const conceptsGroup1 = require('conceptsGroup1.json')

module.exports = {
    props: {
        answers: {
            texts: {}
        },
        activityId: 'Actividad 1',
        
    },
    onEnter: function(){
        swal(_.merge(swalOptions, {
            html: require('templates/slide3Modal.hbs')(),
            customClass: 'ova-themed bigger slide3Modal'
        }))
    },
    addTextareaListeners: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.answers.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.finish').on('click', self.finish)
    },
    renderHexagons: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isObject(args.hexagons))
            throw new Error('Invalid hexagons')

        var self = module.exports
        self.props.answers.selectedCards = args.hexagons
        var index = 0
        var $hexagons = self.$slide.find('.hexagon')

        _.forEach(args.hexagons, function(v, k){
            var src = +k + 32
            var $element = $hexagons.eq(index)
            $element.attr('data-id', v)
                .find('.back').css('background-image', 'url(img/2/' + src + '.png)')

            $element.find('span').html(hexagonTexts[k])
            index++
        })
    },
    finish: function(){
        var self = module.exports
        self.finished = true

        var targetConcepts = _.pick(concepts, [
            'Acción afirmativa',
            'Inclusión Social'
        ])

        var s = swal(_.merge(swalOptions, {
            html: require('templates/slide3AnswerModal.hbs')(),
            customClass: 'ova-themed bigger slide3AnswerModal',
        }))

        self.addModalTextareaListeners()
        $('.swal2-modal .button').on('click', swal.clickConfirm)

        s.then(function(){
            self.props.answers = {
                '0': {
                    '0': self.props.answers.selectedCards,
                    '1': self.props.answers.texts,
                },
            }
        
            return swal(_.merge(swalOptions, {
                html: require('templates/PDFModal.hbs')({
                    pdfArgs: $.param({
                        a: self.props.answers,
                    }),
                    assignLink: _.get(self, 'courseActivities.assign.0.href'),
                }),
                customClass: 'ova-themed bigger ova-send-activity PDFModal',
            }))
        })
        .then(function (t) {
            return self.ovaConcepts.showNewConceptsModal(_.merge({
                concepts: targetConcepts
            }, conceptsGroup1))
        })
        .then(function(){
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })
    
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })
    
            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 1': true,
                },
                action: 'add',
            })
        })

        
    },
    addModalTextareaListeners: function(){
        var self = this
        $('.swal2-modal textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.answers.texts[id] = val
        })
    },
}