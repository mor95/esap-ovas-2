const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const presentation = require('presentation')
const hexagonTexts = require('hexagonTexts.json')

module.exports = {
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide2Modal.hbs')(),
            customClass: 'ova-themed bigger slide2Modal'
        })).then(function(){
            $('.navigation-buttons .next').addClass('highlight')
        })
    },
    props: {
        pickedCards: {}
    },
    selectedCards: {},
    pairedCards: 0,
    enableSelectingMode: function(){
        var self = module.exports
        const slide3 = require('slide3.js')
        
        swal(_.merge(swalOptions, {
            html: require('templates/slide2SelectingModeModal.hbs')(),
            customClass: 'ova-themed bigger slide2SelectingModeModal'
        }))
        .then(function(){
            self.$slide.find('.hexagon').addClass('is-flipped')
        })

        self.$slide.find('.hexagons').addClass('selecting-mode')

        self.$slide.find('.hexagon').off('click').on('click', function(){
            var id = +$(this).attr('data-id')
            if(self.props.pickedCards[id] === true){
                $(this).removeClass('selected')
                delete self.props.pickedCards[id]
            }
            else{
                //if(_.size(self.props.pickedCards) === 3){}

                self.props.pickedCards[id] = true
                $(this).addClass('selected')

                if(_.size(self.props.pickedCards) >= 3){
                    presentation.switchToSlide({
                        slide: 'next'
                    })
                }
            }
            slide3.renderHexagons({
                hexagons: self.props.pickedCards
            })
        })
        
    },
    checkCompleteness: function(){
        var self = this
        if(self.pairedCards >= 3){
            self.$slide.find('.select-cards').addClass('visible')
        }
    },
    addHexagonListeners: function(){
        var self = this
        var $hexagons = self.$slide.find('.hexagon').on('click', function(){
            if(self.disabledCards){
                return true
            }
            var $this = $(this)
            if($this.data('complete')){
                return true
            }

            var id = $this.attr('data-pair-id')
            var state = self.selectedCards
            $this.addClass('is-flipped')
            if(_.size(state) === 0){
                state[id] = {
                    $element: $this
                }
            }
            else{    
                if(!_.isObject(state[id])){//different pair
                    self.disabledCards = true
                    setTimeout(function(){
                        $hexagons.each(function(){
                            if(!$(this).data('complete')){
                                $(this).removeClass('is-flipped')
                            }
                        })
                        self.selectedCards = {}
                        self.disabledCards = false
                    }, 1500)
                }
                else{
                    if($this.is(state[id].$element)){
                        return true//same element
                    }
                    self.disabledCards = true

                    setTimeout(function(){
                        state[id].$element.add($this).addClass('complete').data('complete', true)
                            .each(function(){
                                $(this).children('.back').addClass('glow')
                            })

                        setTimeout(function(){
                            state[id].$element.add($this).each(function(){
                                $(this).children('.back').removeClass('glow') 
                            })
                            delete state[id]
                            self.pairedCards++
                            self.disabledCards = false
                            self.checkCompleteness()
                        }, 500)
                        
                    }, 1500)

                }
            }
        }).on('mouseenter', function(){
            $(this).addClass('animations-disabled')
        })
    },
    randomizeHexagons: function(){
        var self = this
        var $hexagons = self.$slide.find('.hexagon')
        $hexagons.each(function(k){
            var src = k + 32
            $hexagons.eq(k).attr('data-id', k)
                .find('.back').css('background-image', 'url(img/2/' + src + '.png)')
        })

        $hexagons.shuffle()
    },
    addJQueryShuffle: function(){
        $.fn.shuffle = function () {
            var j;
            for (var i = 0; i < this.length; i++) {
                j = Math.floor(Math.random() * this.length);
                $(this[i]).before($(this[j]));
            }
            return this;
        }
    },
    addEventListeners: function(){
        var self = this
        self.addHexagonListeners()
        self.addJQueryShuffle()
        self.randomizeHexagons()
        self.$slide.find('.select-cards').on('click', self.enableSelectingMode)
    }

}