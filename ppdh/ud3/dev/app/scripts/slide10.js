const _ = require('lodash')
const swalOptions = require('swalOptions.js')
const swal = require('sweetalert2')
const presentation = require('presentation')
const hexagonTexts = require('hexagonTexts.json')

module.exports = {
    props: {
        answers: {
            texts: {}
        },
    },
    addTextareaListeners: function(){
        var self = this
        self.$slide.find('textarea').on('keyup paste', function(){
            var val = $(this).val()
            var id = $(this).attr('data-id')
            self.props.answers.texts[id] = val
        })
    },
    addEventListeners: function(){
        var self = this
        self.addTextareaListeners()
    },
    renderHexagons: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isObject(args.hexagons))
            throw new Error('Invalid hexagons')

        var self = module.exports
        self.props.answers.selectedCards = args.hexagons
        var index = 0
        var $hexagons = self.$slide.find('.hexagon')

        _.forEach(args.hexagons, function(v, k){
            var src = +k + 32
            var $element = $hexagons.eq(index)
            $element.attr('data-id', v)
                .find('.back').css('background-image', 'url(img/2/' + src + '.png)')

            $element.find('span').html(hexagonTexts[k])
            index++
        })
    }
}