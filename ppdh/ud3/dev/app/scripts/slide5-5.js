const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup2 = require('conceptsGroup2.json')

module.exports = {
    onEnter: function(){
        $('.navigation-buttons .home').addClass('highlight')
    },
    onSlide: function(){
        var self = module.exports

        var targetConcepts = _.pick(concepts, [
            'Grupos de especial protección',
        ])

        self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup2))
        .then(function(){
    
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })
    
            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Características del manejo de la Información': true,
                },
                action: 'add',
            })
        })

        return true
    },
    addEventListeners: function(){}
}