const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup3 = require('conceptsGroup3.json')

module.exports = {
    onSlide: function(){
        var self = this
        self.finish()
        return true
    },
    finish: function(){
        var self = module.exports

        var targetConcepts = _.pick(concepts, [
            'Marco jurídico de protección de sujetos de especial protección',
        ])

        self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup3))
        .then(function(){
    
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })
    
            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true,
                },
                action: 'add',
            })
        })
    },
    onEnter: function(){
        swal(_.merge(swalOptions, {
            html: require('templates/slide6Modal.hbs')(),
            customClass: 'ova-themed bigger slide6Modal'
        }))
    }, 
    addEventListeners: function(){
        var self = this
    }
}