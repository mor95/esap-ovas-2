<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Gestión Local y Práctica de Los Derechos Humanos</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Autoevaluación</h2>

        <h2>Términos asociados</h2>

        <ol class="terms">
            {foreach from=$a[0] key=$k item=$v}
                <li><b>{$selfQuizTerms[$v]}</b>: {$selfQuizDefinitions[$k]}</li>
            {/foreach}
        </ol>

        <div class="content">
            <h2>Del emparejamiento realizado en el paso 1, escoja tres términos-concepto y descríbalos de manera amplia con el conocimiento que actualmente usted
            maneja sobre este tema.</h2>
            <table>
                <thead>
                    <tr>
                        <th>
                            <span>Término</span>
                        </th>
                        <th>
                            <span>Descripción</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="writable" data-key="0">{$a[1][0]}</td>
                        <td class="writable" data-key="1">{$a[1][1]}</td>
                    </tr>
                    <tr>
                        <td class="writable" data-key="2">{$a[1][2]}</td>
                        <td class="writable" data-key="3">{$a[1][3]}</td>
                    </tr>
                    <tr>
                        <td class="writable" data-key="4">{$a[1][4]}</td>
                        <td class="writable" data-key="5">{$a[1][5]}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
