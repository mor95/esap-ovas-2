<?php
    $selfQuizDefinitions = Array(
        "Acciones estatales",
        "Método para generar igualdad real y equidad",
        "Condiciones que afectan el interés general",
        "Obligaciones estatales",
        "Consiste en la disposición de servicios, materiales, facilidades e infraestructura",
        "Son los instrumentos que permiten conseguir la garantía de realización de los derechos",
        "Sucesión de pazos que permiten posicionar un tema en la agenda pública de manera democrática",
        "El uso de mecanismos legales para conseguir la garantía de realización de un derecho fundamental",
        "Elementos de un plan de incidencia",
        "Tipos de enfoque diferencial"
    )
?>