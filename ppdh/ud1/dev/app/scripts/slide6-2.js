const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const selfQuizDefinitions = require('selfQuizDefinitions.json')
const selfQuizTerms = require('selfQuizTerms.json')
const preciseDraggable = require('precise-draggable')

module.exports = {
    props: {
        texts: {}
    },
    addEventListeners: function(){
        var self = this
        self.addTableFunctions()
        self.$slide.find('.check-terms').on('click', self.viewCompleteTerms)
    },
    deleteCurrentTextarea: function(){
        var self = module.exports
        if(self.props.$textarea instanceof jQuery){
            var $parent = self.props.$textarea.parent()
            var key = $parent.attr('data-index')
            self.props.$textarea.remove()
            $parent.html(self.props.texts[key])
            delete self.props.$textarea
        }
    },
    viewCompleteTerms: function(){
        var self = module.exports
        const slide6_1 = require('slide6-1.js')
        var state = slide6_1.props.dropped
        var terms = {}

        _.forEach(state, function(v, k){
            terms[selfQuizTerms[v]] = selfQuizDefinitions[k]
        })

        swal(_.merge(swalOptions, {
            html: require('templates/definedTerms.hbs')({
                terms: terms
            }),
            customClass: 'ova-themed bigger definedTerms'
        }))
    },
    addTableFunctions: function(){
        var self = this
        
        self.$slide.on('click', self.deleteCurrentTextarea)

        self.$slide.find('.writable').on('click', function(e){
            e.stopPropagation()
            var $this = $(this)
            var key = $this.attr('data-key')
            self.props.$textarea = $('<textarea/>')

            self.props.$textarea.on('keyup paste', function(e){
                if(e.which === 27) return self.deleteCurrentTextarea()
                var val = $(this).val()
                self.props.texts[key] = val
            })
            .on('click', function(e){
                e.stopPropagation()
            })
            .on('focusout', function(){
                self.deleteCurrentTextarea()
                $this.html(self.props.texts[key])
            })
            .val(self.props.texts[key])
            .appendTo($this)
            .focus()
        })
    }
}