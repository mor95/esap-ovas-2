const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const selfQuizDefinitions = require('selfQuizDefinitions.json')
const preciseDraggable = require('precise-draggable')

module.exports = {
    props: {
        dropped: {},
        droppableKey: 0,
    },
    addEventListeners: function(){
        var self = this
        self.addDraggableEvents()
        self.startDroppable(0)
        self.$slide.find('.button.restart').on('click', self.restart)
        self.$slide.find('.next-term').on('click', self.jumpToNextQuestion)
    },
    restart: function(){
        var self = module.exports
        self.props.dropped = {}
        self.$slide.find('.draggable-element').show()
        self.startDroppable(0)
    },
    startDroppable: function(id){
        if(!_.isNumber(id)) id = 0
        if(!_.isString(selfQuizDefinitions[id]))
            throw new Error('Invalid id ' + id)

        var self = this
        var template = require('templates/droppableZone.hbs')

        if(self.props.$droppable instanceof jQuery){
            self.props.$droppable.parent().remove()
        }

        var $element = $(template({
            point: selfQuizDefinitions[id]
        }))
    
        self.$slide.find('.content').prepend($element)
        self.props.$droppable = $element.find('.droppable-zone').data('key', id)

        self.props.droppableKey = id

        self.addDroppableEvents(self.props.$droppable)
        
        if(id === selfQuizDefinitions.length - 1){
            self.$slide.find('.next-term').hide()
        }
        else{
            self.$slide.find('.next-term').show()
        }
    },
    addDraggableEvents: function(){

        var self = this
        var $draggableElements = self.$slide.find('.draggable-element')

        preciseDraggable.currentProps.$stage = $('#stage')

        $draggableElements.each(function () {
            var $draggable = $(this)
            var draggie = preciseDraggable.setDraggable({
                $target: $draggable,
                data: {
                    key: $draggable.index(),
                }
            })

            draggie.on('dragStart', function () {
                preciseDraggable.currentProps.dragging = true
                preciseDraggable.currentProps.lastDraggable = {
                    data: {
                        key: $draggable.index(),
                    },
                    $element: $draggable
                }
                self.props.$droppable.each(function () {
                    if (!$(this).data('dropped')) {
                        $(this).addClass('highlight')
                    }
                })
            })

            draggie.on('dragMove', function (event) {
                draggie.overlap = preciseDraggable.getOverlap({
                    originalGroup: $draggable,
                    matchingGroup: self.props.$droppable
                })

                self.props.$droppable.trigger('draggableLeave')
                if (_.isObject(draggie.overlap)) {
                    draggie.overlap.$element2.trigger('draggableOver')
                }
            })


            draggie.on('dragEnd', function () {
                self.props.$droppable.removeClass('highlight over')
                if (_.isObject(draggie.overlap)) {
                    draggie.overlap.$element2.trigger('draggableDrop')
                }
            })

        })
        
    },
    addDroppableEvents: function($element){
        var self = this
        if(!($element instanceof jQuery))
            throw new Error('Invalid $element')

        $element.on({
            'draggableOver': function () {
                if (preciseDraggable.currentProps.dragging) {
                    if (!$(this).data('dropped')) {
                        $(this).addClass('over')
                    }
                }
            },
            'draggableLeave': function () {
                if (!$(this).data('dropped')) {
                    $(this).removeClass('over')
                }
            },
            'draggableDrop': function () {
                $('#stage').removeClass('grabbing')
                var $droppable = $(this)
                preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging')
                if (!$droppable.data('dropped')) {
                    var element = preciseDraggable.currentProps.lastDraggable
                    var draggableKey = element.data.key
                    var droppableKey = $droppable.data('key')

                    $droppable
                        .data('dropped', true)
                        .html(element.$element.hide().html())

                    self.props.dropped[droppableKey] = draggableKey

                    self.jumpToNextQuestion()

                    /*if(_.size(self.props.draggable) === self.props.$droppable.length){
                        self.finish()
                    }*/
                }

            }
        })
    },
    jumpToNextQuestion: function(){
        var self = module.exports
        if(self.props.droppableKey < selfQuizDefinitions.length - 1){
            self.startDroppable(self.props.droppableKey + 1)
        }
    }
}