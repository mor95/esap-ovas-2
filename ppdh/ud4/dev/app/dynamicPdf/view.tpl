<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Políticas públicas</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>Políticas Públicas y Derechos Humanos</h1>
            <h2>Políticas públicas</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Autoevaluación</h2>

        <h2>Términos asociados</h2>

        <div class="selected-terms">
            {foreach from=$a[0] key=$k item=$v}
                <h2 class="term"><b>{$terms[$v]}:</b> {$definitions[$k]}</h2>
            {/foreach}
        </div>

        <h2>Con el apareamiento realizado, escoja tres términos-concepto y descríbalos de manera amplia con el conocimiento que actualmente usted maneja sobre el tema.</h2>

        <table>
            <thead>
                <tr>
                    <th>TÉRMINO-CONCEPTO</th>
                    <th>DESCRIPCIÓN</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$a[1][0]}}</td>
                    <td>{{$a[1][3]}}</td>
                </tr>
                <tr>
                    <td>{{$a[1][1]}}</td>
                    <td>{{$a[1][4]}}</td>
                </tr>
                <tr>
                    <td>{{$a[1][2]}}</td>
                    <td>{{$a[1][5]}}</td>
                </tr>
            </tbody>
        </table>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
