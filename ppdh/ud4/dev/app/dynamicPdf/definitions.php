<?php
    $definitions = Array(
        "Asuntos del gobierno, la comunidad y las empresas",
        "Conjunto de acciones que representan a un grupo social",
        "Situación que afecta a un grupo social",
        "Forma de medir y valorar el alcance de un objetivo",
        "Forma como se convierte un problema en una solución",
        "Pasos que se adoptan para el desarrollo de una política pública",
        "Forma como se valora la implementación, formulación y/o evaluación de una política pública"
    )
?>