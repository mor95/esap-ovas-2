const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup2 = require('conceptsGroup2.json')

module.exports = {
    onEnter: function(){
        $('.navigation-buttons .home').addClass('highlight')
    },
    onSlide: function(){
        var self = this
        var targetConcepts = _.pick(concepts, [
            'Identificación de problemas públicos',
            'Enfoques en las políticas públicas',
            'Ciclo de las políticas públicas'
        ])

        self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup2))
        .then(function (t) {
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Tema 2: Ciclo de políticas públicas': true
                },
                action: 'add'
            })
        })

        return true
    },
    addEventListeners: function(){
        var self = this
        
    }
}