const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')

module.exports = {
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide8Modal.hbs')(),
            customClass: 'ova-themed bigger slide8Modal'
        })).then(function(){
            $('.naviation-buttons .next').addClass('highlight')
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.number').on('click', function(){
            var $text = $(this).siblings('.text')
            $text.addClass('visible')
            var $parent = $(this).parent()
            if($parent.next().length){
                $parent.next().find('.number').addClass('visible')
            }
        })
    }
}