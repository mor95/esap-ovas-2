const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup2 = require('conceptsGroup2.json')

module.exports = {
    props: {
        seenContents: {},
        answers: {}
    },
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide13Modal.hbs')(),
            customClass: 'ova-themed bigger slide13Modal'
        }))
        .then(function(){
            return swal(_.merge(swalOptions, {
                html: require('templates/slide13Modal2.hbs')(),
                customClass: 'ova-themed bigger slide13Modal2'
            }))
        })
    },
    addContentEvents: function(){
        var self = this
        self.$slide.find('.circle').on('click', function(){
            if(!$(this).children().length){
                $(this).append('<i class="fa fa-check"/>')
            }
            var id = $(this).attr('data-id')
            self.props[id] = true
            self.$slide.find('.content')
                .removeClass('visible')
                .filter('[data-id="' + id + '"]')
                .addClass('visible')
        })
    },
    addTextEvents: function(){
        var self = this
        var $collection = self.$slide.find('textarea, input[type="text"]')
        $collection.on('keyup paste', function(){
            var val = $(this).val()
            var index = $collection.index($(this))
            console.log('index', index)
            self.props.answers[index] = val
        })
    },
    finish: function(){
        var self = module.exports
        return swal(_.merge(swalOptions, {
            html: require('templates/PDFModal.hbs')({
                pdfArgs: $.param({
                    a: self.props.answers
                }),
                assignLink: _.get(self, 'courseActivities.forum.0.href'),
                activityId: 2,
                pdfId: 2
            }),
            customClass: 'ova-themed bigger PDFModal ova-send-activity',
        }))
        .then(function(){
            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true,
                },
                action: 'add',
            })
        })
    },
    addEventListeners: function(){
        var self = this
        self.addContentEvents() 
        self.addTextEvents() 
        self.$slide.find('.finish').on('click', self.finish)
    }
}