const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')

module.exports = {
    onEnter: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide14Modal.hbs')(),
            customClass: 'ova-themed bigger slide14Modal'
        })).then(function(){
            $('.naviation-buttons .next').addClass('highlight')
        })
    },
    addEventListeners: function(){

    }
}