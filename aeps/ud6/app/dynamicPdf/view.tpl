<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Estándar de gestión te tecnologías y gerencia de la información</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }
    @font-face {
        font-family: 'BebasNeue';
        src: url('../fonts/BebasNeue.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFBold';
        src: url('../fonts/FF_DIN_Pro_Bold.otf');
    }
    @font-face {
        font-family: 'FFBCondBold';
        src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
    }
    @font-face {
        font-family: 'FFCondRegular';
        src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
    }
    @font-face {
        font-family: 'FFCondLight';
        src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
    }
    @font-face {
        font-family: 'FFLight';
        src: url('../fonts/FF_DIN_Pro_Light.otf');
    }
    @font-face {
        font-family: 'FFThin';
        src: url('../fonts/FF_DIN_Pro_Thin.otf');
    }
    @font-face {
        font-family: 'Wingding';
        src: url('../fonts/wingding.ttf');
    }
    @font-face {
        font-family: 'century';
        src: url('../fonts/CenturyGothic.ttf');
    }
    @font-face {
        font-family: 'gothiddd';
        src: url('../fonts/GOTHIC.ttf');

    }
    @font-face {
        font-family: 'myriadRegular';
        src: url('../fonts/MyriadPro-Regular.otf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'gothiddd';
    }

    h2 {
        font-family: 'gothiddd';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #005B60;
    }

    p {
        font-size: 20px;
        color:#005B60;
        font-family: 'gothiddd';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        background-color: #005B60;
        padding: 10px;
        border-radius: 20px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #005B60;
        color: #FFFFFF;
        padding: 10px;
        border-radius: 10px;
    }

    header h1 {
        font-family: 'gothiddd';
        font-size: 20px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFF;
        padding-left: 80px;

    }

    header h2 {
        font-family: 'gothiddd';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }
    #esap-logo {
        position: absolute;
        width: 60px;
    }
    .question-text {
        font-family: 'FFDINPro-Light';
        color: #005B60;
        font-size: 16px;
    }
    .date {
        font-family: 'FFDINPro-Light';
        color: #005B60;
        text-align: right;
        font-size: 8px;
        border-bottom: solid #005B60 1px dashed;
        margin-bottom: 10px;
    }
    .date p {
        font-size: 12px;
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }
    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }
    .pregunta-title{
      width: 100%;
      height: auto;
      padding: 5px;
    }
    .word-cloud {
      width: 90%;
      min-height: 40%;
      position: relative;
      margin: 0 auto;
      text-align: center;
      background: url(assets/img/nube-2.png);
      background-position: 50%;
      background-repeat: no-repeat;
      background-size: 100%;
    }
    .word-cloud li{
      font-family: 'FFDINPro-Light';
      font-size:1.4em;
      margin: 0px;
      padding: 2px;
      border: solid 2px #005B60;
      position: relative;
      text-align: center;
      list-style: none;
      display: inline;
    }
</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>Estándar de gestión te tecnologías y gerencia de la información</h1>
        <h2>Acreditación para Entidades Prestadoras de Salud</h2>
    </header>
    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">{$r2}</h2>
    <br>

    <div class="word-cloud">
      <li style="font-size:{$r1[0]};">Medicamentos</li>
      <li style="font-size:{$r1[1]};">Equipos</li>
      <li style="font-size:{$r1[2]};">Tecnología</li>
      <li style="font-size:{$r1[3]};">Farmacovigilancia</li>
      <li style="font-size:{$r1[4]};">Biomédicos</li>
      <li style="font-size:{$r1[5]};">Adquisición</li>
      <li style="font-size:{$r1[6]};">Monitorización</li>
      <li style="font-size:{$r1[7]};">Política</li>
      <li style="font-size:{$r1[8]};">Médicos</li>
      <li style="font-size:{$r1[9]};">Dispositivos</li>
      <li style="font-size:{$r1[10]};">Reposición</li>
      <li style="font-size:{$r1[11]};">Confidencialidad</li>
      <li style="font-size:{$r1[12]};">Decisiones</li>
      <li style="font-size:{$r1[13]};">Información</li>
      <li style="font-size:{$r1[14]};">Estandarización</li>
      <li style="font-size:{$r1[15]};">Control</li>
      <li style="font-size:{$r1[16]};">Sistemas</li>
      <li style="font-size:{$r1[17]};">Idenficación</li>
      <li style="font-size:{$r1[18]};">Soporte</li>
      <li style="font-size:{$r1[19]};">Contingencia</li>
      <li style="font-size:{$r1[20]};">Seguridad</li>
      <li style="font-size:{$r1[21]};">Mejoramiento</li>
    </div>


    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
