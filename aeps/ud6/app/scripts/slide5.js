'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    onEnter:function(){
      $('.button-item').addClass("animated pulse").css("animation-iteration-count" , "infinite");
    },
    addEventListeners: function () {
        var self = this;
        $('.button-item').on('click' ,  function(){
          var $databtn = $(this).data("btn");
          console.log($databtn);
          if($databtn == 3){
            $('.btn_cont_S5').css('display' , 'block');
            }
          $('.info-item[data-infoitem="' +$databtn+ '"]').css("display", "block");
          $('.info-item[data-infoitem="' +$databtn+ '"]').addClass("animated bounceInUp");
        })

        $('.btn_cont_S5').on("click", function(){
          var targetConcepts = _.pick(concepts, [
            "Equipo biomédico de tecnología controlada",
            "Estándar"
          ]);
          self.ovaConcepts.showNewConceptsModal({
              concepts: targetConcepts,
              title: 'Responda mentalmente la siguiente pregunta',
              subtitle: '¿Cuál de los grupos de gestión de información causaría más dificultades en caso de que no se le diera el manejo adecuado?'

          }).then(function (value) {
              presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
              });
              self.ovaConcepts.updateConcepts({
                  concepts: targetConcepts,
                  action: "insert",
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              });
              self.ovaProgress.updateFinishedTopics({
                  topics: {
                      'Recursos 3': true
                  },
                  action: 'add',
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              })
          })
        })

    }
}
