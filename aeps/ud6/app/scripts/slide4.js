'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    addEventListeners: function () {
        var self = this;
        $('.btn_cont_S4').on('click', function(){
          self.ovaProgress.updateFinishedTopics({
              topics: {
                  'Recursos 2': true
              },
              action: 'add',
              courseShortName: courseData.shortName,
              courseUnit: courseData.unit
          })
        })
    }
}
