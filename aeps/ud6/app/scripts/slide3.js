'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    addEventListeners: function () {
        var self = this;
        $(".btn_cont_S3").on("click" , function(){
            var targetConcepts = _.pick(concepts, [
              "Acreditación en salud",
              "Calidad"
            ]);
            self.ovaConcepts.showNewConceptsModal({
                title: 'Responda mentalmente la siguiente pregunta:',
                subtitle: '¿Puede citar algunos ejemplos de dispositivos con clasificación IIB?',
                concepts: targetConcepts
            }).then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaConcepts.updateConcepts({
                    concepts: targetConcepts,
                    action: "insert",
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Recursos 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
            })
        })
    }
}
