<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        img{
            width:100%;
        }

        @font-face {
            font-family: 'BebasNeue';
            src: url('assets/fonts/BebasNeue.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Bold';
            src: url('assets/fonts/FFDINPro-Bold.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Light';
            src: url('assets/fonts/FFDINPro-Light.ttf');
        }

        @font-face {
            font-family: 'FFDINPro-Thin';
            src: url('assets/fonts/FFDINPro-Thin.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Bold';
            src: url('assets/fonts/FFDINProCond-Bold.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Light';
            src: url('assets/fonts/FFDINProCond-Light.ttf');
        }

        @font-face {
            font-family: 'FFDINProCond-Regular';
            src: url('assets/fonts/FFDINProCond-Regular.ttf');
        }

        @font-face {
            font-family: 'MyriadPro-Regular';
            src: url('assets/fonts/MyriadPro-Regular.ttf');
        }

        @font-face {
            font-family: 'wingding';
            src: url('assets/fonts/wingding.ttf');
        }

        @font-face {
            font-family: 'Wingdings-Regular';
            src: url('assets/fonts/Wingdings-Regular.ttf');
        }

        html{
        }
        
        body{
            font-size:12px;
            font-family:'MyriadPro-Regular';
        }
        h2{
            font-family:'FFDINProCond-Bold';
            font-size:20px;
            font-weight:100;
            text-align:center;
            color:#102A19;
        }
        p{
            font-size:20px;
            color:#102A19;
            font-family:'MyriadPro-Regular';
        }

        
        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;
            background-color:#88BD2B;
            padding:10px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            font-size:12px;
            font-family:'FFDINProCond-Bold';
            text-align: center;
            background-color:#88BD2B;
            color:#FFFFFF;
            padding:10px;
        } 

        header h1{
            font-family:'FFDINProCond-Bold';
            font-size:20px;
            font-weight:100;
            margin:0;
            padding:0;
            text-align: left;
            color:#102A19;
            padding-left:80px;
        }

        header h2{
            font-family:'FFDINProCond-Bold';
            font-size:16px;
            font-weight:100;
            margin:0;
            padding:0;
            text-align: left;
            color:#FFFFFF;
            padding-left:80px;

        }

        #esap-logo{
            position:absolute;
            width:60px;
        }

        .question-text{
            font-family:'FFDINPro-Light';
            color:#6BB42C;
            font-size:16px;
        }

        .answer-text{
            font-family:'FFDINPro-Light';
            color:#3AAA35;
            font-size:16px;
            margin-left:16px;
            background-color:#D6DDC0;
            padding:5px;
        }

        .date{
            font-family:'FFDINPro-Light';
            color:#6BB42C;
            text-align:right;
            font-size:8px;
        }

        .date p{
            font-size:12px;
        }

        .date b{
            font-family:'FFDINPro-Bold';
            font-weight:inherit;
        }

        .quiz-title{
            font-family:'FFDINPro-Light';
            margin:0 18px 0 10px;
            padding:0;
            font-size:24px;
        }

        .caja{
            margin-top: 5%;
            background-color: #DBB295;
        }

        .texto{
            display: inline;
            color: white;
        }

        .respuesta{
            display: inline;
            /*margin: 0.5% 1%;*/
            color: #BE0C51;
        }
    </style>
    <body>
        <header>
            <img src="../img/esap_logo.png" id="esap-logo">
            <h1>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</h1>
            <h2>ESTÁNDARES DEL PROCESO DE ATENCIÓN AL CLIENTE ASISTENCIAL</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">ACTIVIDAD DE AUTOEVALUCIÓN FINAL</h2>
        <p>Estos resultados de autoevaluación del aprendizaje le permiten verificar la apropiación de los conceptos y a la vez evidenciar
        aquellos que debe fortalecer. Compare sus respuestas y si es necesario revise el banco de conceptos y desarrolle
        las actividades del aula virtual.</p>
        <br>
        {foreach from=$options item=$item key=$key}
            <div class="question">
                <p class="question-text">{$key+1}. {$item}</p>
                {foreach from=$pdfArgs item=object key=k}
                        {if $k == $key}
                            <p class="answer-text">{$object['answer']}</p>
                        {/if}                     
                {/foreach}
            </div>  
        {/foreach}
        <!-- {foreach from=$options item=$item key=$key}
            <div class="question">
                <p class="question-text">{$key+1}. {$item}</p>
                {foreach from=$pdfArgs item=object key=k}
                    {if $k == $key}
                        <div class="old">
                            <h3>Respuesta inicial</h3>
                            <p class="answer-text">{$object['initialAnswers']}</p>
                        </div>
                        <div class="new">
                            <h3>Respuesta final</h3>
                            <p class="answer-text">{$object['finalAnswers']}</p>
                        </div>
                    {/if}
                {/foreach}
            </div>
        {/foreach} -->

        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
