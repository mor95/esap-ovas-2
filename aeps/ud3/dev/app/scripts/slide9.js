const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

	onEnter: function(){
		swal({
		    target: stageResize.currentProps.$stage.get(0),
		    allowOutsideClick: false,
		    allowEscapeKey: false,
		    customClass: 'ova-themed',
		    showCloseButton: true,
		    showConfirmButton: false,
		    title: 'En cada casilla observará la cantidad de estándares aplicables a cada proceso  de atención al cliente.',
		}).then(function (t) {
				
		});
	},

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='9'] .paso").on("click", function () {
        	$this = $(this);
        	$this.addClass("visto");
        	$paso = $this.data("paso");

        	setTimeout(function () {
        		$this.find("span").removeClass("oculto");
        	}, 300);
        	setTimeout(function () {
        		$("[data-slide='9'] .flecha[data-flecha='" + $paso + "']").removeClass("oculto");
        	}, 1000);
        	setTimeout(function () {
        		$this.next().removeClass("oculto");
        	}, 1300);
        	setTimeout(function () {
        		if ($("[data-slide='9'] .visto").length == $("[data-slide='9'] .paso").length) {
        		    $("[data-slide='9'] .button-continuar").removeClass("escondido");
        		}
        	}, 2300);
        });

        $("[data-slide='9'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Usuario",
        	    "Plan de mejora"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "Conteste mentalmente la siguiente pregunta",
        	    subtitle: "¿Existe algún estándar que usted agregaría a los grupos estudiados?",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_4': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $('.slide[data-slide="1"] .estrella[data-estrella="6"]').addClass("dorada"); 
        	    }, 3000);
        	})
        });
	}
}