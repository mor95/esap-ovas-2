const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    currentProps: {
        answers: []
    },

	onEnter: function(){
		swal({
		    target: stageResize.currentProps.$stage.get(0),
		    allowOutsideClick: false,
		    allowEscapeKey: false,
		    customClass: 'ova-themed',
		    showCloseButton: true,
		    showConfirmButton: false,
		    title: 'En la siguiente actividad observará con mayor profundidad las características requeridas en  los procesos de atención asistencial.',
		}).then(function (t) {
				
		});
	},

    addEventListeners: function(){

    	var self = this;

    	$('.slide[data-slide="8"]  textarea').on("keyup", function () {
            $this = $(this);
            $this.addClass("visto");
            $respuesta = $this.data("respuesta");

            module.exports.currentProps.answers[$respuesta-1] = $this.val();

            if ($("[data-slide='8'] .visto").length == $("[data-slide='8'] textarea").length) {
                $("[data-slide='8'] .foro").removeClass("oculto");
            }
        });

        $("[data-slide='8'] .foro").on("click", function () {

            console.log(module.exports.currentProps.answers);

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.answers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.answers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });
        	swal({
        	    target: stageResize.currentProps.$stage.get(0),
        	    allowOutsideClick: false,
        	    allowEscapeKey: false,
        	    customClass: 'ova-themed modalActividad2',
        	    showCloseButton: true,
        	    showConfirmButton: false,
        	    html: require("templates/slide8Pdf.hbs")(
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.1.href')
                        }
                    ),
        	}).then(function (t) {
        		$("[data-slide='8'] .button-continuar").removeClass("escondido");	
        	});
        });

        $("[data-slide='8'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Confidencialidad"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "Conteste mentalmente la siguiente pregunta",
        	    subtitle: "¿Qué impacto tienen los puntos en común encontrados en el ejercicio anterior?",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'actividad_2': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $('.slide[data-slide="1"] .estrella[data-estrella="5"]').addClass("dorada"); 
        	    }, 3000);
        	})
        });
	}
}