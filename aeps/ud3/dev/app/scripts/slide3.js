const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    currentProps: {
        oldAnswers: []
    },

    addEventListeners: function(){

        var self = this;

        var initialAnswers = [];

        $('.slide[data-slide="3"]  textarea').on("keyup", function () {
            $this = $(this);
            $this.addClass("visto");
            $respuesta = $this.data("respuesta");

            initialAnswers[$respuesta-1] = $this.val();

            if ($("[data-slide='3'] .visto").length == $("[data-slide='3'] textarea").length) {
                $("[data-slide='3'] .button-continuar").removeClass("escondido");
            }
        });

        $('.slide[data-slide="3"] .button-continuar').on("click", function () {

            console.log(initialAnswers);

            module.exports.currentProps.oldAnswers = initialAnswers;
            console.log(module.exports.currentProps.oldAnswers);

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluacion primera',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.oldAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            });

            cargarRespuestasIniciales();

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.oldAnswers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.oldAnswers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });



            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide3Pdf.hbs")(
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.0.href')
                        }
                    ),
            }).then(function (t) {
                var targetConcepts = _.pick(concepts, [
                    "Estándar",
                    "Grupos de estándares"
                ]);
                
                self.ovaConcepts.showNewConceptsModal({
                    title: "Conteste mentalmente la siguiente pregunta",
                    subtitle: "¿Cuál considera Usted, es el elemento más importante en la atención al cliente?",
                    concepts: targetConcepts
                })
                .then(function (t) {
                    presentation.switchToSlide({
                      slide: $('.slide[data-slide="1"]')
                    });
                    setTimeout(function () {
                        self.ovaConcepts.updateConcepts({
                            concepts: targetConcepts,
                            action: "insert",
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 1000);
                    setTimeout(function () {
                        self.ovaProgress.updateFinishedTopics({
                            topics: {
                                'evaluacion_inicial': true
                            },
                            action: 'add',
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 2000);
                    setTimeout(function () {
                        $('.slide[data-slide="3"]').data("presentation").enabled = false;
                        $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");
                    }, 3000);
                    setTimeout(function () {
                        $('.slide[data-slide="1"] .learning-experience').removeClass("disabled");
                        $('.slide[data-slide="1"] .estrella').removeClass("clicked");
                        $('.slide[data-slide="4"]').data("presentation").enabled = true; 
                    }, 4000);
                })
            });
        });

        function cargarRespuestasIniciales(){
            for (var i = 0; i < module.exports.currentProps.oldAnswers.length; i++) {
                $('.slide[data-slide="11"]  textarea[data-respuesta="' + (i+1) + '"]').text(module.exports.currentProps.oldAnswers[i]);
            }
        }
    }
}