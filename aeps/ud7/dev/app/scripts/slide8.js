const presentation = require('presentation')
const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const conceptsGroup3 = require('conceptsGroup3.json')
const concepts = require('concepts.json')

module.exports = {
    props: {
        seenModals: {}
    },
    onEnter: function(){
        swal(_.merge(swalOptions, {
            html: require('templates/slide8Modal.hbs')(),
            customClass: 'ova-themed biggest slide8Modal'
        }))
    },
    finish: function(){
        var self = module.exports
        self.finished = true

        var targetConcepts = _.pick(concepts, [
            'Sistema Obligatorio de Garantía de la Calidad en Salud',
            'Sistema Único de Habilitación'
        ])

        return self.ovaConcepts.showNewConceptsModal(_.merge({
            concepts: targetConcepts
        }, conceptsGroup3))
        .then(function (t) {
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            })

            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Resumen de la unidad': true
                },
                action: 'add'
            })
        })
    },
    showModal: function(id){
        if(!_.isNumber(id) && !_.isString(id))
            throw new Error('Invalid id')

        var self = this
        return swal(_.merge(swalOptions, {
            html: require('templates/slide8Modal' + id + '.hbs')({
                argument: 1
            }),
            customClass: 'ova-themed biggest slide8Modal slide8Modal' + id
        }))
        .then(function(){
            self.props.seenModals[id] = true
            if(_.size(self.props.seenModals) === 4){
                self.finish()
            }
            if(id == 1){
                return self.showModal('1-1')
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.$slide.find('.item').on('click', function(){
            var id = $(this).attr('data-id')
            self.showModal(id)
        })

    }
}