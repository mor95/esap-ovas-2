const ovaAnimateElements = require('ova-animate-elements')
const animatePerSlide = require('animate-per-slide')
const _ = require('lodash')
const swal = require('sweetalert2')
const swalOptions = require('swalOptions.js')
const concepts = require('concepts.json')
const conceptsGroup1 = require('conceptsGroup1.json')
const conceptsGroup2 = require('conceptsGroup2.json')
const activitiesHandler = require('activitiesHandler.js')

module.exports = {
    props: {
        slidesWithActivities: [3, 10],
        finishedSlides: {}
    },
    onEnter: function(){
        var self = this

        _.forEach(self.props.slidesWithActivities, function(v){
            var slide = require('slide' + v + '.js')
            if(!slide.finished || self.props.finishedSlides[v] === true){
                return true
            }

            self.props.finishedSlides[v] = true
            var activityId = slide.props.activityId
            activitiesHandler.add({
                activityId: activityId,
                answers: slide.props.answers,
                APIInstance: self.APIInstance
            })
            .then(function(response){
                console.log('response', response)
                if(response !== true){
                    return swal(_.merge(swalOptions, {
                        html: require('templates/activitySavingError.hbs')({
                            activityId: activityId
                        }),
                        customClass: 'ova-themed bigger activitySavingError visible-icons',
                        type: 'error'
                    }))
                }

                return self.renderActivityIcon({
                    activityId: activityId,
                    answers: slide.props.answers
                })
            })
        })
    },
    renderActivityIcon: function(args){
        if(!_.isObject(args))
            throw new Error('Invalid arguments')

        if(!_.isString(args.activityId) && !_.isNumber(args.activityId))
            throw new Error('Invalid activityId')

        if(!_.isObject(args.answers))
            throw new Error('Invalid answers')

        var self = this
        var $anchor = self.$slide.find('a[data-activity="' + args.activityId + '"]')
        if(!$anchor.length){
            console.info('No anchor was found for activity ' + args.activityId)
            return false
        }

        var _href = $anchor.attr('_href')
        var href = _href + '?' + $.param({a: args.answers})
        $anchor.attr('href', href).removeClass('disabled')
    },
    animateSlides: function(){
        var self = this
        $('.slide').each(function(){
            ovaAnimateElements($(this).children())
        })

        animatePerSlide.setAnimationClasses({
            $slide: self.$slide,
            action: 'add'
        })
    },
    showInitialModal: function(){
        var self = this
        swal(_.merge(swalOptions, {
            html: require('templates/slide1Modal.hbs')(),
            customClass: 'ova-themed bigger slide1Modal'
        }))
    },
    showVideoModal1: function(){
        var self = module.exports
        var targetConcepts = _.pick(concepts, [
            'Calidad',
            'Mejora continua'
        ])

        swal(_.merge(swalOptions, {
            html: require('templates/videoModal.hbs')({
                videoLink: _.get(self, 'media.AN.mediaid')
            }),
            customClass: 'ova-themed bigger video'
        }))
        .then(function(){
            return self.ovaConcepts.showNewConceptsModal(_.merge({
                concepts: targetConcepts
            }, conceptsGroup1))
        })
        .then(function (t) {
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    '14 puntos clave para la mejora continua': true
                },
                action: 'add'
            })
        })
    },
    showVideoModal2: function(){
        var self = module.exports
        var targetConcepts = _.pick(concepts, [
            'Proceso',
            'Sistema de gestión',
            'Acreditación en salud'
        ])

        swal(_.merge(swalOptions, {
            html: require('templates/videoModal.hbs')({
                videoLink: 'o22Cs8kZ6sE'//hard-coded media.AV.mediaid
            }),
            customClass: 'ova-themed bigger video'
        }))
        .then(function(){
            return self.ovaConcepts.showNewConceptsModal(_.merge({
                concepts: targetConcepts
            }, conceptsGroup2))
        })
        .then(function (t) {
            self.ovaConcepts.updateConcepts({
                concepts: targetConcepts,
                action: 'insert',
            })

            return self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Sistema único de acreditación': true
                },
                action: 'add'
            })
        })
    },
    parseSavedActivities: function(){
        var self = this
        _.forEach(['Actividad 1', 'Actividad 3'], function(v){
            if(_.isObject(self.activities[v])){
                self.renderActivityIcon({
                    activityId: v,
                    answers: self.activities[v]
                })
            }
        })
    },
    addEventListeners: function(){
        var self = this
        self.animateSlides()
        self.showInitialModal()
        self.$slide.find('.hover-me').on('mouseenter', function(){
            $(this).removeClass('animated')
        })
        self.$slide.find('.experience-menu[data-id="0"] .black-square[data-id="2"]').on('click', self.showVideoModal1)
        self.$slide.find('.experience-menu[data-id="1"] .black-square[data-id="0"]').on('click', self.showVideoModal2)
        self.parseSavedActivities()
    }
}