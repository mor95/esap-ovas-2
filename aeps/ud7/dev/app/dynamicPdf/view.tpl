<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        {include file='base.css'}
        {include file='styles.css'}
    </style>
    <body>
        <header>
            <img src="assets/img/esap_logo.png" id="esap-logo">
            <h1>ACREDITACIÓN PARA ENTIDADES PRESTADORAS DE SALUD</h1>
            <h2>ESTÁNDARES DE MEJORAMIENTO DE LA CALIDAD</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">¿Qué sé acerca del Sistema Obligatorio de garantía de la calidad de la salud?</h2>
  
        <h2 class="content">Entre otros aspectos, vale la pena destacar los siguientes elementos del enfoque de
        mejoramiento y algunos problemas prácticos que hemos detectado*:<br>
        El mejoramiento de la <b>{$o[$a[0]]}</b> surge de un cuidadoso <b>{$o[$a[1]]}</b> de
        <b>{$o[$a[2]]}</b>, en el que se <b>{$o[$a[3]]}</b> la calidad esperada
        (<b>{$o[$a[4]]}</b>) con la <b>{$o[$a[5]]}</b> real de la <b>{$o[$a[6]]}</b>.<br>
        Dicho <b>{$o[$a[7]]}</b> de <b>{$o[$a[8]]}</b> debe ser <b>{$o[$a[9]]}</b> (<b>{$o[$a[10]]}</b>,
        periódico, <b>{$o[$a[11]]}</b>) y <b>{$o[$a[12]]}</b>, es decir, que comprenda a la
        <b>{$o[$a[13]]}</b> como un <b>{$o[$a[14]]}</b>, incluyendo los diferentes
        <b>{$o[$a[15]]}</b> y los <b>{$o[$a[16]]}</b>, así como las diferentes
        <b>{$o[$a[17]]}</b> del <b>{$o[$a[18]]}</b>, desde la <b>{$o[$a[19]]}</b> de la salud y
        la <b>{$o[$a[20]]}</b> de la <b>{$o[$a[21]]}</b>, el <b>{$o[$a[22]]}</b> y tratamiento, hasta
        la <b>{$o[$a[23]]}</b>.<br></h2>
        <br>
        <h2 class="content">El <b>{$o[$a[24]]}</b> puede surgir como <b>{$o[$a[25]]}</b> de una o varias
        <b>{$o[$a[26]]}</b>, pero tiene un <b>{$o[$a[27]]}</b>. En efecto, ocurre después de un
        <b>{$o[$a[28]]}</b> de la situación y como fruto del <b>{$o[$a[29]]}</b> de los
        <b>{$o[$a[30]]}</b>, que son la <b>{$o[$a[31]]}</b> del éxito del proceso de
        <b>{$o[$a[32]]}</b> y no de <b>{$o[$a[33]]}</b> individuales aisladas.<br>
        </h2>
        <br>
        <h2 class="content">El mejoramiento debe abarcar tanto la <b>{$o[$a[34]]}</b> como los procesos y la
        <b>{$o[$a[35]]}</b> de <b>{$o[$a[36]]}</b>. Un proceso de mejora centrado tan sólo en uno de
        los <b>{$o[$a[37]]}</b>, por ejemplo la <b>{$o[$a[38]]}</b>, constituye una
        visión <b>{$o[$a[39]]}</b>.
        </h2>

        <h2>Envíe este documento al aula por medio del botón "Enviar actividad" de la pestaña anterior que lo llevará al aula del curso. Luego de enviar sus respuestas cierre la pestaña del aula y siga explorando el recurso en la pestaña inicial.</h2>
        
        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
