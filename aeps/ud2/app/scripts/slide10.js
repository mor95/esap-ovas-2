'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
const slide2 = require('slide2.js');

module.exports = {
  onEnter:function(args){
    var self = this;
    console.log(self.activities);
    if (_.isObject(self.activities.AutoevaluacionPrimera.textos.f1)) {
      console.log(this);
      var algo = self.activities.AutoevaluacionPrimera.textos.f1;
      console.log("llega del servidor", algo)
      $('.cont-textd textarea').val(algo);
    }else{
      console.log(this);
      var $textareasend = slide2.currentProps.textareasend
      console.log("llega de la variable", $textareasend);
      $('.cont-textd textarea').val($textareasend);
    }
    if (_.isObject(self.activities.AutoevaluacionPrimera.textos.f2)) {
      var algobueno = self.activities.AutoevaluacionPrimera.textos.f2;
      console.log("llega del servidor", algo)
      $('input').each(function(){
        var algodos = $(this).attr('value')
        if(algobueno == algodos){
          $(this).prop('checked' , true);
        }
      })
    }else{
      var $textinputsend = slide2.currentProps.textinputsend
      console.log("llega de la variable", $textinputsend);
      $('input').each(function(){
        var algodos = $(this).attr('value')
        if($textinputsend == algodos){
          $(this).prop('checked' , true);
        }
      })
    }
  },
    addEventListeners: function () {
        var self = this;
        var texto_actunod
        var textodos_actunod
        $('input').click(function() {
          $('input').prop('checked' , false);
          $(this).prop('checked' , true);
        })
        $('.button-finishd').on("click", function(){
          $('input:checked').each(function(){
            texto_actunod = $(this).val()
            console.log('valor' , texto_actunod);
          })
          textodos_actunod = $('.cont-textd textarea').val();
          console.log('texto', textodos_actunod);
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_3.hbs")({
                  parametros: $.param({
                    r1: texto_actunod,
                    r2: textodos_actunod
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.1.href')
                })
              })
              .then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.button-act[data-button-act="3"]').removeClass("animate-active").addClass("disabled");

            })
        })

    }
}
