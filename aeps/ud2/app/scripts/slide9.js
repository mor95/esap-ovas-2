'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
      currentProps: {
          seenSlides: {},
          finished: false
      },
    addEventListeners: function () {
        var self = this;
        $('.btn_modal-s8').on("click", function(){
          var dataModal=$(this).data("modal");
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed biggest modal_s8',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_s8_" + dataModal +".hbs")()
            }).then(function () {
                module.exports.currentProps.seenSlides[dataModal] = true;
                module.exports.checkModals();
            })
        })
    },
    checkModals: function () {
      var self = this;

        if (module.exports.currentProps.finished == true) {
            return true;
        }
        if (_.size(module.exports.currentProps.seenSlides) == 2) {
              var targetConcepts = _.pick(concepts, [
                "SOGCS",
                "Sistema único de habilitación",
                "Tecnovigilancia"
              ]);
              self.ovaConcepts.showNewConceptsModal({
                  concepts: targetConcepts,
                  title: 'Leálos e intente recordar su definición:',
                  subtitle: '¿Son estos conceptos parte de su contexto laboral habitual?'

              }).then(function (value) {
                  presentation.switchToSlide({
                      slide: $('.slide[data-slide="1"]')
                  });
                  self.ovaConcepts.updateConcepts({
                      concepts: targetConcepts,
                      action: "insert",
                      courseShortName: courseData.shortName,
                      courseUnit: courseData.unit
                  });
                  self.ovaProgress.updateFinishedTopics({
                      topics: {
                          'Recursos 4': true
                      },
                      action: 'add',
                      courseShortName: courseData.shortName,
                      courseUnit: courseData.unit
                  })
              })
        }
    }
}
