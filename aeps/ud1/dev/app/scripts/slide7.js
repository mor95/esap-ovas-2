const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

	onEnter: function(){
		swal({
		    target: stageResize.currentProps.$stage.get(0),
		    allowOutsideClick: false,
		    allowEscapeKey: false,
		    customClass: 'ova-themed',
		    showCloseButton: true,
		    showConfirmButton: false,
		    title: '¿Qué es la acreditación en salud? Es un proceso voluntario, al que se someten las entidades de salud de manera voluntaria y periódica, donde las mismas entidades evalúan sus políticas y gestión con el fin de mejorar la prestación de su servicio, explore algunas de sus características a continuación.',
		});
	},

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='7'] .opcion").on("click", function () {
        	$this = $(this);
        	$this.addClass("visto");
        	$opcion = $this.data("opcion");
        	setTimeout(function () {
        		$this.removeClass("noColor");
        	}, 200);
        	setTimeout(function () {
        		$("[data-slide='7'] .info[data-info='" + $opcion + "']").removeClass("oculto");
        	}, 700);
        	setTimeout(function () {
        		if ($("[data-slide='7'] .visto").length == $("[data-slide='7'] .opcion").length) {
        			setTimeout(function () {
        				$("[data-slide='7'] .consultorio").removeClass("noColor");
        			}, 200);
        			setTimeout(function () {
        				$("[data-slide='7'] .button-continuar").removeClass("escondido");
        			}, 700);
        		}
        	}, 1200);
        });

        $("[data-slide='7'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Acreditación en Salud"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "¿Qué forma podría tener una descripción gráfica del proceso de acreditación de salud?",
        	    subtitle: "Busque en internet imágenes correspondientes a los procesos de acreditación de salud. Después compare su imagen con 1º Las imágenes de internet, 2º La definición que encuentra en el cofre de conceptos. Responda para usted ¿Qué tan diferente es mi imagen de esta definición? ",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_5': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $('.slide[data-slide="1"] .recurso[data-recurso="5"]').addClass("visto");
        	    }, 3000);
        	})
        });
	}
}