const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

        var self = this;

        $("[data-slide='3'] .opcion").on("click", function () {
            $this = $(this);
            $this.addClass("visto");
            $opcion = $this.data("opcion");

            setTimeout(function () {
                $("[data-slide='3'] .tema[data-tema='" + $opcion + "']").removeClass("oculto");
            }, 300);
            setTimeout(function () {
                $("[data-slide='3'] .flecha[data-flecha='" + $opcion + "']").removeClass("oculto");
            }, 700);
            setTimeout(function () {
                $("[data-slide='3'] .info[data-tema='" + $opcion + "']").removeClass("oculto");
            }, 1100);
            setTimeout(function () {
                $this.next().removeClass("escondido");
            }, 2500);

            setTimeout(function () {
                if($opcion == 5)
                    $("[data-slide='3'] .decreto").removeClass("escondido");
            }, 3000);

            setTimeout(function () {
                if ($("[data-slide='3'] .visto").length == $("[data-slide='3'] .opcion").length) {
                    $("[data-slide='3'] .continuar").removeClass("oculto");
                }
            }, 3500);
        });

        $("[data-slide='3'] .continuar").on("click", function () {
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed video',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide3Video.hbs")(self.media),
            }).then(function (t) {
                var targetConcepts = _.pick(concepts, [
                    "Seguridad social"
                ]);
                
                self.ovaConcepts.showNewConceptsModal({
                    title: "Teniendo en cuenta el contenido de video, ¿qué imagen se le viene a la mente cuando piensa en la “Seguridad social”",
                    subtitle: "Desde su experiencia como usuario de EPS elabore mentalmente un relato donde incluya las palabras Seguridad social, después revise su cofre de conceptos para comparar su relato mental con el concepto.",
                    concepts: targetConcepts
                })
                .then(function (t) {
                    presentation.switchToSlide({
                      slide: $('.slide[data-slide="1"]')
                    });
                    setTimeout(function () {
                        self.ovaConcepts.updateConcepts({
                            concepts: targetConcepts,
                            action: "insert",
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 1000);
                    setTimeout(function () {
                        self.ovaProgress.updateFinishedTopics({
                            topics: {
                                'recurso_1': true
                            },
                            action: 'add',
                            courseShortName: courseData.shortName,
                            courseUnit: courseData.unit
                        });
                    }, 2000);
                    setTimeout(function () {
                        $('.slide[data-slide="1"] .recurso[data-recurso="1"]').addClass("visto"); 
                    }, 3000);
                })
            });
        });
    }
}