const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const draggables = require("draggables.json");
const preciseDraggable = require('precise-draggable');
const slidesManager = require('slidesManager.js');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed',
            showCloseButton: true,
            showConfirmButton: false,
            title: '¿Cuáles son los componentes del  SOGCS? Empareje coherentemente los conceptos y descúbralo.',
        });
    },

    addEventListeners: function(){

    	var self = this;

        _.forEach(draggables.droppableZones, function (v, k) {
            $('<div class="droppable drop" data-ani-class="wobble"/>')
            .appendTo($('.slide[data-slide="4"] .contenedor[data-n="2"]'))
            .on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = v.target;//$(this).attr('data-key')
                        console.log("drop target " + droppableKey);
                        console.log("drag target " + draggableKey);
                         if(draggableKey == droppableKey){
                         	$(this).removeClass('over');
	                        $(this).addClass('dropeado caja');
	                        // if(draggableKey != droppableKey){
	                        //     return true;
	                        // }
	                        $(this).data('dropped', true);
	                        var $el = preciseDraggable.currentProps.lastDraggable.$element;
	                        $el.addClass("caja");
	                        $(this).append($el.html());
	                        $el.remove();
	                        if ($("[data-slide='4'] .dropeado").length == $("[data-slide='4'] .drop").length) {
	                            $("[data-slide='4'] .contenedor[data-n='4']").removeClass("oculto");

	                            var i = 1;
							    interval = setInterval(function () {
							    	$("[data-slide='4'] .linea[data-n='" + i + "']").removeClass("escondido");
							    	$("[data-slide='4'] .cajita[data-cajita='" + i + "']").removeClass("escondido");
							        console.log(i++);  // this is inside your loop
							        if(i == 7){
							        	clearInterval(interval);
							        	$("[data-slide='4'] .button-continuar").removeClass("escondido");
							        }
							    }, 600);
	                        }  
                        }
                    }
                    
                }
            });
        });

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){

            var $draggableItems = $('.slide[data-slide="4"] .draggable-items'); //toma d7iv 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="caja draggable-element" data-caja="' + k +'" data-key="' + v.target + '"><div class="grande"><span>' + v.label + '</span></div><div class="peque fs-bg ' + v.imagen + '"></div></div>')
                    .appendTo($('.slide[data-slide="4"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='4'] .button-continuar").on("click", function () {
            var targetConcepts = _.pick(concepts, [
                "Sistema Obligatorio de Garantía de la calidad de la Salud (SOGCS)"
            ]);
            
            self.ovaConcepts.showNewConceptsModal({
                title: "¿Qué componente llamó más su atención? ¿Por qué?",
                subtitle: "Busque en internet imágenes asociadas al Sistema Obligatorio de Garantía de la calidad de la Salud, luego revise su cofre de conceptos y verifique la coherencia de su búsqueda con la definición encontrada.",
                concepts: targetConcepts
            })
            .then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
                setTimeout(function () {
                    self.ovaConcepts.updateConcepts({
                        concepts: targetConcepts,
                        action: "insert",
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1000);
                setTimeout(function () {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'recurso_2': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 2000);
                setTimeout(function () {
                    $('.slide[data-slide="1"] .recurso[data-recurso="2"]').addClass("visto");
                }, 3000);
            })
        });

	}
}