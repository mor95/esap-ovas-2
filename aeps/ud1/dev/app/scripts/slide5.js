const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

	onEnter: function(){
		audio = document.getElementById('audios');
		audio.src = "img/5/AEPS_U1_0.mp3";
        audio.currentTime = 1;
        audio.load();
        audio.play();
	},

    addEventListeners: function(){

    	var self = this;
    	var playing = false;
    	
    	var audio = document.getElementById('audios');

        $("[data-slide='5'] .opcion").on("click", function () {
        	if (!playing) {
        		playing = true;
	        	$this = $(this);
	        	$this.addClass("visto");
	        	$opcion = $this.data("opcion");

	        	setTimeout(function () {
	        		$this.removeClass("noColor");
	        	}, 200);
	        	setTimeout(function () {
	        		$("[data-slide='5'] .flecha[data-flecha='" + $opcion + "']").removeClass("oculto");
	        	}, 500);
	        	setTimeout(function () {
	        		$("[data-slide='5'] .titulo[data-titulo='" + $opcion + "']").removeClass("oculto");
	        	}, 800);
	        	setTimeout(function () {
	        		audio.currentTime = 0;
	        		audio.src = "img/5/AEPS_U1_" + $opcion + ".mp3";
			        audio.load();
			        audio.play();

			        audio.onended = function() {
		                playing = false;

		                if ($("[data-slide='5'] .visto").length == $("[data-slide='5'] .opcion").length) {
		                    $("[data-slide='5'] .button-continuar").removeClass("escondido");
		                }
		            };
	        	}, 900);
	        }
        });

        $("[data-slide='5'] .button-continuar").on("click", function () {
        	presentation.switchToSlide({
        	  slide: $('.slide[data-slide="1"]')
        	});
        	setTimeout(function () {
        		self.ovaProgress.updateFinishedTopics({
        		    topics: {
        		        'recurso_3': true
        		    },
        		    action: 'add',
        		    courseShortName: courseData.shortName,
        		    courseUnit: courseData.unit
        		});
        	}, 1000);
        	setTimeout(function () {
        		$('.slide[data-slide="1"] .recurso[data-recurso="3"]').addClass("visto");
        	}, 2000);
        });
	}
}