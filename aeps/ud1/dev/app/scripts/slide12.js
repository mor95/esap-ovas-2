const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed',
            showCloseButton: true,
            showConfirmButton: false,
            title: 'Actividad de aprendizaje 3. ¿Qué aprendí del Sistema  Obligatorio de Garantía de la Calidad de la Salud? tenga la infografía que realizó para la actividad 1 a la mano y complémentela o modifíquela si lo considera necesario.',
        });
    },

    addEventListeners: function(){

        var self = this;

        $("[data-slide='12'] .opcion").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                $this = $(this);
                $this.addClass("visto");
                
                if ($("[data-slide='12'] .visto").length == $("[data-slide='12'] .opcion").length - 1) {
                    $("[data-slide='12'] .guardar").removeClass("disabled");
                }

                if ($(this).hasClass("guardar")) {
                    $("[data-slide='12'] .enlace").attr("href", _.get(self, 'courseActivities.assign.2.href'));
                    continuar();
                }
            }
        });

        function continuar() {
            setTimeout(function () {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
            }, 1500);

            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'actividad_final': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 3000);
            setTimeout(function () {
                $("[data-slide='1'] .actividad[data-actividad='1']").addClass("disabled clicked");
                $('.slide[data-slide="2"]').data("presentation").enabled = false;
                $("[data-slide='1'] .actividad[data-actividad='3']").addClass("disabled clicked");
                $('.slide[data-slide="12"]').data("presentation").enabled = false;
            }, 4500);
        }
    }
}