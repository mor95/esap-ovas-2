const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

	currentProps: {
	    backgrounds: ["amarillo", "aguamarina", "verde", "morado", "purpura", "cafe"],
	    parejas: 1,
        intentos: 0
	},

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed',
            showCloseButton: true,
            showConfirmButton: false,
            title: '¿Cuáles son los ejes centrales de la acreditación en salud? Empareje coherentemente los conceptos y descúbralo.',
        }).then(function (t) {
                
        });
    },

    addEventListeners: function(){

    	var validando = false;

    	var self = this;

        $("[data-slide='11'] .pista").on("click", function () {
            $aleatorio = Math.floor((Math.random() * 4) + 1);
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed slide11Pista',
                showCloseButton: true,
                showConfirmButton: false,
                html: '<img src="../img/11/pista' + $aleatorio + '.png">',
            });
        });

        $("[data-slide='11'] .caja").on("click", function () {
        	$this = $(this);
        	$caja = $this.data("caja");
        	if (!$this.hasClass("emparejado")  && !$this.hasClass("comprobando") && !validando) {
        		console.log("entra");
        		$this.find(".cajita").removeClass("oculto");
        		if ($("[data-slide='11'] .comprobando").length != 0) {
                    
                    $("[data-slide='11'] .filtro").removeClass("oculto");
        			validando = true;
        			
        			primerTarjeta = $("[data-slide='11'] .comprobando");
        			segundaTarjeta = $this;
        		    
        		    valorPrimerTarjeta = $("[data-slide='11'] .comprobando").data("pareja");
        		    console.log("primer " + primerTarjeta);
        		    valorSegundaTarjeta = $this.data("pareja");
        		    console.log("segunda " + segundaTarjeta);

        		    if (valorPrimerTarjeta == valorSegundaTarjeta) {
        		    	//reproduzca sonido
        		    	primerTarjeta.addClass("emparejado");
        		    	segundaTarjeta.addClass("emparejado");

        		    	primerTarjeta.find(".cajita").addClass(module.exports.currentProps.backgrounds[module.exports.currentProps.parejas-1]);
        		    	segundaTarjeta.find(".cajita").addClass(module.exports.currentProps.backgrounds[module.exports.currentProps.parejas-1]);

        		    	module.exports.currentProps.parejas++;
        		    	$("[data-slide='11'] .comprobando").find("cajita").addClass("oculto");
        		    	$("[data-slide='11'] .comprobando").removeClass("comprobando");

        		    	audio = document.getElementById('correcto');
        		    	audio.src = "img/11/correcto.wav";
				        audio.load();
				        audio.play();

                        $("[data-slide='11'] .filtro").addClass("oculto");

        		    	if ($("[data-slide='11'] .emparejado").length == $("[data-slide='11'] .caja").length) {
        		    	    $("[data-slide='11'] .button-continuar").removeClass("escondido");
        		    	}

        		    	validando = false;
        		    }else{
        		    	$this.find(".cajita").removeClass("oculto");
        		    	setTimeout(function () {
        		    		audio = document.getElementById('incorrecto');
        		    		audio.src = "img/11/incorrecto.wav";
					        audio.load();
					        audio.play();
        		    		$("[data-slide='11'] .comprobando").find(".cajita").addClass("oculto");
	        		    	$("[data-slide='11'] .comprobando").removeClass("comprobando");
	        		    	$this.find(".cajita").addClass("oculto");
	        		    	validando = false;
                            $("[data-slide='11'] .filtro").addClass("oculto");
                            module.exports.currentProps.intentos++;
                            if(module.exports.currentProps.intentos == 5)
                                $("[data-slide='11'] .pista").removeClass("oculto");
        		    	}, 3000);
        		    }
        		}else{
        			$this.addClass("comprobando");
        		}
        	}
        });

        $("[data-slide='11'] .button-continuar").on("click", function () {
        	var targetConcepts = _.pick(concepts, [
        	    "Sistema Único de Habilitación"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "¿Cuál eje cree usted que se vulnera más en nuestra sistema de salud?",
        	    subtitle: "desde su experiencia como usuario del sistema de salud, ubique tres situaciones en las que hubiese visto vulnerado uno o varios ejes; analícelas tomando como referencia la información presentada en el curso y como considera usted que debería haberse dado trámite a asertivo  la situación.",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'recurso_7': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	    setTimeout(function () {
        	        $('.slide[data-slide="1"] .recurso[data-recurso="7"]').addClass("visto");
        	    }, 3000);
        	})
        });
	}
}