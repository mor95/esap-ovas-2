const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");

module.exports = {

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='5'] .explorar").on("click", function () {
        	presentation.switchToSlide({
        	  slide: $('.slide[data-slide="1"]')
        	});
        });

        $("[data-slide='5'] .foro").on("click", function () {
            $("[data-slide='5'] .enlace").attr("href", _.get(self, 'courseActivities.assign.1.href'));
            $("[data-slide='5'] .button-continuar").removeClass("escondido");
        });

        $("[data-slide='5'] .button-continuar").on("click", function () {

        	var targetConcepts = _.pick(concepts, [
        	    "Lugar de trabajo",
        	    "Riesgo"
        	]);
        	
        	self.ovaConcepts.showNewConceptsModal({
        	    title: "¿Qué potenciales riesgos se encuentran en su lugar de trabajo?",
        	    subtitle: "Imagine que tiene un medidor de riesgos laborales con diferentes escalas. Mentalmente intente aplicar este medidor a su lugar de trabajo para detectar el nivel de riesgo en que se encuentra como bajo, medio o alto. Después consulte estos conceptos en el cofre y si es necesario ajuste la imagen mental.",
        	    concepts: targetConcepts
        	})
        	.then(function (t) {
        	    presentation.switchToSlide({
        	      slide: $('.slide[data-slide="1"]')
        	    });
        	    setTimeout(function () {
        	        self.ovaConcepts.updateConcepts({
        	            concepts: targetConcepts,
        	            action: "insert",
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 1000);
        	    setTimeout(function () {
        	        self.ovaProgress.updateFinishedTopics({
        	            topics: {
        	                'actividad_2': true
        	            },
        	            action: 'add',
        	            courseShortName: courseData.shortName,
        	            courseUnit: courseData.unit
        	        });
        	    }, 2000);
        	})
        });
	}
}