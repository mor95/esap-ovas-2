'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
const draggables = require("draggables.json");
const preciseDraggable = require('precise-draggable')
const slidesManager = require('slidesManager.js')

module.exports = {
    currentProps: {
        newAnswers: []
    },
    addEventListeners: function () {
        var self = this;

        var finalAnswers = [];

        _.forEach(draggables.droppableZones, function (v, k) {
            $('<div class="droppable drop" data-ani-class="wobble" data-posicion="' + k +'"/>')
            .appendTo($('.slide[data-slide="7"] .caja'))
            .css({
                'top': v.y + '%',
                'left': v.x + '%'
            }).on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $(this).addClass('dropeado');
                        if(draggableKey !== droppableKey){
                            return true;
                        }
                        $(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;
                        $(this).append($el.html());
                        $el.remove();

                        console.log($el.data("palabra"));
                        finalAnswers[k] = $el.data("palabra");
                        // if(self.$slide.find('.draggable-element').length === 0){
                            
                        // }
                        $("[data-slide='7'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        });

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){

            var $draggableItems = $('.slide[data-slide="7"] .draggable-items'); //toma div 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="palabra draggable-element" data-palabra="' + k +'" data-ani-class="wobble" data-ani-delay="700"><span>' + v.label + '</span></div>')
                    .appendTo($('.slide[data-slide="7"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='7'] .reiniciar").on("click", function () {
            $("[data-slide='7'] .drop").html("");
            $("[data-slide='7'] .drop").removeClass("dropeado");
            $("[data-slide='7'] .drop").data('dropped', false);

            console.log(finalAnswers);
             finalAnswers = [];
             console.log(finalAnswers);

             cargarDrags();

             $(this).addClass("oculto");
        });

        $("[data-slide='7'] .guardar").on("click", function () {
            module.exports.currentProps.newAnswers = finalAnswers;
            console.log(module.exports.currentProps.newAnswers);

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluacion final',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.newAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            });

            var pdfArgs = {};
            _.forEach(draggables.droppableZones, function(v, k){

                pdfArgs[k] = {
                  answer: module.exports.currentProps.newAnswers[k]
                };
            });

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });

            $("[data-slide='7'] .generarPdf").attr("href", "dynamicPdf/pdfFinal.php?" + pdfArgs);

            $("[data-slide='7'] .enviar").removeClass("oculto");
        });

        $("[data-slide='7'] .enviar").on("click", function () {
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide7Enviar.hbs")
                (
                    {
                        assignLink: _.get(self, 'courseActivities.assign.2.href')
                    }
                ),
            }).then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
                setTimeout(function () {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'evaluacion_final': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1000);
                setTimeout(function () {
                    $("[data-slide='1'] .actividad[data-actividad='3']").addClass("disabled");
                    $('.slide[data-slide="6"]').data("presentation").enabled = false;
                }, 2000);
            });
        });
    }
}


