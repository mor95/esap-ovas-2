const _ = require('lodash')

module.exports = {
    rectCheck: function(args){
        if(!_.isObject(args)){
            throw new Error('Some arguments were expected.')
        }
        if(!args.element1 instanceof Element){
            throw new Error('An element was expected as property element1.');
        }
        if(!args.element2 instanceof Element){
            throw new Error('An element was expected as property element2');
        }
        var rect1 = args.element1.getBoundingClientRect();
        var rect2 = args.element2.getBoundingClientRect();

        return !(rect1.right < rect2.left || 
            rect1.left > rect2.right || 
            rect1.bottom < rect2.top || 
            rect1.top > rect2.bottom);
    },
    areOverlapping: function(args){
        var self = this;
        if(!_.isObject(args)){
            throw new Error('Some arguments were expected.')
        }
        
        if(typeof args.originalGroup === 'undefined'){
            throw new Error('A group was expected in property originalGroup');
        }

        if(!args.originalGroup instanceof jQuery){
            throw new Error('A jQuery collection was expected.');
        }

        if(!args.originalGroup.length){
            throw new Error('Original group has no length. Elements are non-existent in DOM.');
        }

        if(typeof args.matchingGroup === 'undefined'){
            throw new Error('A group was expected in property matchingGroup');
        }

        if(!args.matchingGroup instanceof jQuery){
            throw new Error('A jQuery collection was expected.');
        }

        if(!args.matchingGroup.length){
            throw new Error('Original group has no length. Elements are non-existent in DOM.');
        }

        var operlapping = false;

        args.originalGroup.each(function(){
            var $el = $(this);
            console.log('$el', $el);
            args.matchingGroup.each(function(){
                var $_el = $(this);
                console.log('$_el', $_el);
                if(self.rectCheck({
                    element1: $el.get(0),
                    element2: $_el.get(0)
                })){
                    overlapping = {
                        $element1: $el,
                        $element2: $_el
                    }
                    return false;
                }
            })
            if(_.isObject(overlapping)){
                return false;
            }
        });

        return overlapping;
    }
}