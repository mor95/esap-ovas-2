'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    addEventListeners: function () {
        var self = this;
        $(".btn_cont_S10").on("click" , function(){
            var targetConcepts = _.pick(concepts, [
              "Plan de mejora",
              "Proceso",
              "Usuario"
            ]);
            self.ovaConcepts.showNewConceptsModal({
                title: 'Lea cada uno de estos conceptos e intente recordar  su definición',
                subtitle: 'Está su dependencia o área vinculada a estos conceptos? Como se presenta esta relación?',
                concepts: targetConcepts
            }).then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaConcepts.updateConcepts({
                    concepts: targetConcepts,
                    action: "insert",
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Recursos 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
            })
        })
    }
    }
