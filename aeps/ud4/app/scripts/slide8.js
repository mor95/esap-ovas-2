'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    addEventListeners: function () {
        var self = this;
        $(".btn_cont_S8").on("click" , function(){
            var targetConcepts = _.pick(concepts, [
            "Estándar",
            "Grupo de estándares",
            "Humanización del servicio"
            ]);
            self.ovaConcepts.showNewConceptsModal({
                title: 'Lea cada uno de estos conceptos y recuerde lo que entiende de cada uno de ellos',
                subtitle: 'Piense en qué estado o nivel de cumplimiento puede encontrarse la institución a la que usted pertenece, respecto a estos conceptos.',
                concepts: targetConcepts
            }).then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaConcepts.updateConcepts({
                    concepts: targetConcepts,
                    action: "insert",
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Recursos 2': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
            })
        })
    }
    }
