'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json')
var $araytablauno= new Array(11)
var $araytablados= new Array(11)
var $arrayargument= new Array(11)
module.exports = {
  addEventListeners: function () {
    var self = this;

// COLUMN ONE
    var $selectunocount = $('.select-colum-1').length;
    for (var i = 0; i < $selectunocount; i++) {
      console.log("valor de i" , i);
      $('.select-colum-1').each(function(){
          $(this).attr('data-selectonepos', i++);
      })
    }
    $('.select-colum-1 select').change(function(){
      var $valonepos = $(this).parent().data('selectonepos');
      var $valone = $(this).val()
      $araytablauno[$valonepos] = $valone;
      console.log("array column-1" , $araytablauno);
    })
// COLUMN TWO
    var $selectdoscount = $('.select-colum-2').length;
    for (var a = 0; a < $selectdoscount; a++) {
      console.log("valor de a" , a);
      $('.select-colum-2').each(function(){
          $(this).attr('data-selecttwopos', a++);
      })
    }

    $('.select-colum-2 select').change(function(){
      var $valtwopos = $(this).parent().data('selecttwopos');
      var $valtwo = $(this).val()
      $araytablados[$valtwopos] = $valtwo;
      console.log("array column-2" , $araytablados);
    })

// ARGUMENT
    var $argumentcount = $('.item-input').length;
    for (var b = 0; b < $argumentcount; b++) {
      console.log("valor de b" , b);
      $('.item-input').each(function(){
          $(this).attr('data-argumentpos', b++);
      })
    }

    $('.item-input textarea').change(function(){
      var $valposargument = $(this).parent().data('argumentpos');
      var $valargument = $(this).val()
      $arrayargument[$valposargument] = $valargument
      console.log("array argument" , $arrayargument);
    })

    $('.button-finish-act2').on("click", function(){
        console.log("array column-1" , $araytablauno);
        console.log("array column-2" , $araytablados);
        console.log("array argument" , $arrayargument);
        swal({
            target: stageResize.currentProps.$stage.get(0),
            customClass: 'ova-send-activity',
            showCloseButton: true,
            showConfirmButton: false,
            html: require("templates/modal_act_2.hbs")({
              parametros: $.param({
                r1: $araytablauno,
                r2: $araytablados,
                r3: $arrayargument
              }),
              assignLink: _.get(self, 'courseActivities.forum.0.href')
            })
          })
          .then(function (value) {
            presentation.switchToSlide({
                slide: $('.slide[data-slide="1"]')
            });
            self.ovaProgress.updateFinishedTopics({
                topics: {
                    'Actividad 2': true
                },
                action: 'add',
                courseShortName: courseData.shortName,
                courseUnit: courseData.unit
            })
        })
    })

  }
}
