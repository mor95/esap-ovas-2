'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
const slide2 = require('slide2.js');
module.exports = {

  onEnter: function(){
    var self = this;

    if (_.isObject(self.activities.AutoevaluacionPrimeraUd4.textos.r3)) {
      var a=-1;
      var rtauno = self.activities.AutoevaluacionPrimeraUd4.textos.r3;
      console.log(rtauno);
      $('.cont-text-tres textarea').each(function(){
        a++
        $(this).val(rtauno[a])
      })
    }else{
      var actpartuno  = slide2.currentProps.$textosactunosend
      console.log("sdfasdfasd", actpartuno);
      var a=-1;
      $('.cont-text-tres textarea').each(function(){
        a++
        $(this).val(actpartuno[a])
      })
    }
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed biggest modal_ini_act_3',
        showCloseButton: false,
        showConfirmButton: false,
        html: require("templates/modal_ini_act_3.hbs")()
      })
      $('.boton_iniciar').on('click', function(){
        swal.close();
      });


  },
    addEventListeners: function () {
        var self = this;
        var $textosacttres = []
        var nomact = "Respuesta Actividad 3"
        $('.button-finish-s11').on("click", function(){
          $('.cont-text-tres textarea').each(function(){
            var escrito = $(this).val();
            $textosacttres.push(escrito);
            console.log($textosacttres);
          });
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_3.hbs")({
                  parametros: $.param({
                    r1: $textosacttres,
                    a1: nomact
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.1.href')
                })
              })
              .then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.button-act[data-button-act="3"]').removeClass("animate-active").addClass("disabled");

            })
        })

    }
}
