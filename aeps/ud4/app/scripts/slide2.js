'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
var $textosactuno = new Array()
module.exports = {
  currentProps: {
      $textosactunosend:0
  },
  onEnter: function(){
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed biggest modal_ini_act_1',
        showCloseButton: false,
        showConfirmButton: false,
        html: require("templates/modal_ini_act_1.hbs")()
      })
      $('.boton_iniciar').on('click', function(){
        swal.close();
      });
  },
    addEventListeners: function () {
        var self = this;
        var nomact = "Respuesta Actividad 1"
        $('.button-finish').on("click", function(){
          $('.cont-text textarea').each(function(){
            var escrito = $(this).val();
            $textosactuno.push(escrito);
            console.log($textosactuno);
            module.exports.currentProps.$textosactunosend = $textosactuno;

          });
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: $textosactuno,
                    a1: nomact
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.0.href')
                })
              })
              .then(function (value) {
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $(".button-exp").removeClass("disabled");
                $(".initial-message").css("display", "none");
                $('.button-act[data-button-act="1"]').removeClass("animate-active").addClass("disabled");

                require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                  component: 'activities',
                  method: 'addLOActivities',
                  showFeedback: true,
                  arguments: {
                      shortName: require('courseData.json').shortName,
                      unit: require('courseData.json').unit,
                      activityId: 'AutoevaluacionPrimeraUd4',
                      answers: JSON.stringify({
                          textos: {
                            r3: $textosactuno
                          }
                      })
                  }
              })
              .then(function(response){
                  console.log('response', response)
              })

            })
        })

    }
}
