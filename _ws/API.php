<?php

/**
 * Punto de entrada de la API
 *
 * @author Christian Arias <cristian1995amr@gmail.com>
 *
 * @version v1.0.1
 *
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);
require '../config.php';
require 'vendor/autoload.php';
require 'APIClass.php';
require 'CustomException.php';

try {
    $API = new API(array(
        'data' => $_REQUEST,
        'DB'   => $DB,
        'CFG' => $CFG
    ));
} catch (Exception $e) {
    echo json_encode(array(
        'status' => 'error',
        'title' => 'Error while creating class.',
        'message' => $e->getMessage() . '.',
        'errorCode' => method_exists($e, 'getCustomCode') ? $e->getCustomCode() : $e->getCode(),
    ));
    exit();
}

try {
    $response = $API->processRequest($_REQUEST);
}
catch (CustomException $e){
    echo json_encode(array(
        'status' => 'error',
        'title' => 'Error while executing method!',
        'message' => $e->getMessage() . '.',
        'errorCode' => method_exists($e, 'getCustomCode') ? $e->getCustomCode() : $e->getCode(),
        'description' => $e->getDescription()
    ));
    exit();
}


try {
    echo str_replace('\\/', '/', json_encode($response));
} catch (Exception $e) {
    echo json_encode(array(
        'status' => 'error',
        'title' => 'Critical error',
        'message' => $e->getMessage() . '.',
        'errorCode' => $e->getCode(),
    ));
}

exit();
