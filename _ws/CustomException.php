<?php

/**
 * customException.php
 * @author Christian Arias <cristian1995amr@gmail.com>
 */

/**
 * Clase para generar excepciones con códigos personalizados.
 *
 * @author Christian Arias <cristian1995amr@gmail.com>
 *
 * @version v1.0.1
 *
 */

class CustomException extends \Exception{
    /**
     * Título de la excepción.
     * @var array
     */
    private $title;

    /**
     * Código alfanumérico de la excepción.
     * @var array
     */
    private $customCode;

    /**
     * Descripción de la excepción.
     * @var array
     */
    private $description;

    /**
     * Código numérico de la excepción.
     * @var array
     */
    protected $code;

    /**
     * Constructor de la clase.
     * Se asignan propiedades de la clase.
     * @param string $title Título de la excepción
     * @param string $customCode Código alfanumérico de la excepción
     * @param string $description Descripción del error
     * @param string $code Código numérico del error
     * @param null $previous Excepción
     */

    public function __construct($title = 'Error', $customCode = '', $description = '', $code = 0, Exception $previous = null){
        parent::__construct($title, $code, $previous);
        $this->title = $title;
        $this->customCode = $customCode;
        $this->description = $description;
        $this->code = $code;
    }

    /**
    * Este método obtiene el título de la excepción
    * @return string
    */

    public function getTitle(){
        return $this->title;
    }

    /**
    * Este método obtiene el código de error con el que se instanció la clase
    * @return string
    */

    public function getCustomCode(){
        return $this->customCode;
    }

    /**
    * Este método obtiene la descripción del error
    * @return string
    */

    public function getDescription(){
        return $this->description;
    }
}

?>