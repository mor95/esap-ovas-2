<?php
trait Activities{

    /**
     * Este método se encarga de obtener las respuestas dadas por el usuario  dentro del OVA en base al usuario
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *unit* y la propiedad *test*
     * la propiedad *test* se utiliza para identificar la autoevaluacion inicial=1, final=2
     *
     * @api
     *
     * @throws CustomException __8x000__, si ocurre un error en la obtención de datos.
     * @throws CustomException __8x001__, si no existen respuestas registradas.
     * @throws CustomException __8x002__, si el usuario no aparenta estar asociado al curso.
     *
     * @return array
     */

    protected function getLOActivities(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric',
	        'activityId' => 'string',
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        if (!is_string($userId) || !is_string($courseId)) {
            throw new CustomException("Course with ID {$courseId} wasn't found for user with ID {$userId}.", "8x002");
        }

        $sql = "SELECT activityid, answer
                  FROM mdl_activity_test
                 WHERE userid   = ?
                   AND courseid = ?
                   AND unit     = ?";

        $sqlArguments = array(
            $userId,
            $courseId,
            $arguments["unit"],
        );
                
        if (array_key_exists("activityId", $arguments)){
            $sql .= " AND activityid = ?";
            array_push($sqlArguments, $arguments["activityId"]);
        }
                
        try{
            $answers = $this->DB->get_records_sql($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while fetching activities test answers.", "8x000");
        }

        if (is_array($answers)) {
            $response = Array();
            foreach($answers as $key => $value){
                $response[$value->activityid] = json_decode($value->answer, true);
            }
            return $response;
        } 

        throw new CustomException("No answers were found for user {$userId} in course {$arguments['shortName']} with ID {$courseId} and unit {$arguments['unit']}", "8x001");
    }

    /**
     * Este método se encarga de adicionar las respuestas a una autoevaluacion realizada dentro del OVA
     * @param array $arguments
     * 
     * @api
     *
     * @throws CustomException __9x000__ si existe un error en la introducción de datos.
     * 
     * @return bool
     */

    protected function addLOActivities(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'answers' => 'required|valid_json_string'
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $this->deleteLOActivities($arguments);

        $sql = "INSERT INTO mdl_activity_test
                            (activityid, userid, courseid, unit, answer)
                    VALUES __VALUES__";

        $values = array();
        $sqlArguments = array();
        array_push($sqlArguments, 
            $arguments['activityId'],
            $userId, 
            $courseId,
            $arguments['unit'],
            trim(preg_replace('/\\\\/', '', preg_replace('/\s\s+/', ' ', $arguments['answers'])))
        );

        array_push($values, "(?, ?, ?, ?, ?)");

        $sqlValues = join(', ', $values);
        $sql = str_replace('__VALUES__', $sqlValues, $sql);

        try{
            $this->DB->Execute($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while adding answers", "9x000");
        }

        return true;
    }

    /**
     * Este método se encarga de eliminar las respuestas a una autoevaluacion por el usuario en el OVA
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *unit* y *test*.
     * la propiedad *test* se utiliza para identificar la autoevaluacion inicial=1, final=2
     *
     * @api
     *
     * @throws CustomException __10x000__, si ocurre un error en la eliminación de información a través del query
     *
     * @return bool
     */

    protected function deleteLOActivities(array $arguments = array()){
        $this->validateArguments($arguments, array(
      		'unit' => 'required|numeric',
            'activityId' => 'string',
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $sql = "DELETE
                  FROM mdl_activity_test
                 WHERE userid   = ?
                   AND courseid = ?
                   AND unit     = ?
			       AND activityId = ?";

        try{
            $this->DB->Execute($sql, array(
                $userId,
                $courseId,
                $arguments["unit"],
			    $arguments["activityId"],
			));
        }
        catch(Exception $e){
            throw new CustomException("There was an error while deleting answers", "10x000");
        }

        return true;
    }
}
?>
