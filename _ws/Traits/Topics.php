<?php
trait Topics{

    /**
     * Este método se encarga de obtener los temas finalizados dentro del OVA en base al usuario
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *unit*.
     *
     * @api
     *
     * @throws CustomException __8x000__, si ocurre un error en la obtención de datos.
     * @throws CustomException __8x001__, si no existen temas asociados al curso.
     * @throws CustomException __8x002__, si el usuario no aparenta estar asociado al curso.
     *
     * @return array
     */

    protected function getLOFinishedTopics(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric'
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        if (is_string($userId) && is_string($courseId)) {
            $sql = "SELECT topic
                        FROM mdl_activities_topics
                        WHERE userid   = ?
                        AND courseid = ?
                        AND unit     = ?";

            try{
                $topics = $this->DB->get_records_sql($sql, array(
                    $userId,
                    $courseId,
                    $arguments["unit"],
                ));
            }
            catch(Exception $e){
                throw new CustomException("There was an error while fetching topics.", "8x000");
            }

            if (is_array($topics)) {
                return $topics;
            } 

            throw new CustomException("No topics were found for user {$userId} in course {$arguments['shortName']} with ID {$courseId} and unit {$arguments['unit']}", "8x001");
        
        }
            
        throw new CustomException("Course with ID {$courseId} wasn't found for user with ID {$userId}.", "8x002");
    }

    /**
     * Este método se encarga de adicionar un tema visto, o una actividad realizada dentro del OVA
     * @param array $arguments
     * 
     * @api
     *
     * @throws CustomException __9x000__ si existe un error en la introducción de datos.
     * 
     * @return bool
     */

    protected function addLOFinishedTopics(array $arguments = array()){
        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $this->deleteLOFinishedTopics($arguments);

        $sql = "INSERT INTO mdl_activities_topics
                            (topic, userid, courseid, unit)
                    VALUES __VALUES__";

        $values = array();
        $sqlArguments = array();
        foreach ($arguments['topics'] as $key => $value) {
            array_push($sqlArguments, $value, $userId, $courseId, $arguments['unit']);
            array_push($values, "(?, ?, ?, ?)");
        }

        $valuesSQL = join(', ', $values);
        $sql = str_replace('__VALUES__', $valuesSQL, $sql);

        try{
            $this->DB->Execute($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while adding topics in course", "9x000");
        }

        return true;
    }

    /**
     * Este método se encarga de eliminar un tema visto por el usuario en el OVA
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *topics*.
     *
     * @api
     *
     * @throws CustomException __10x000__, si ocurre un error en la eliminación de información a través del query
     *
     * @return bool
     */

    protected function deleteLOFinishedTopics(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'topics' => 'required|array',
            'unit' => 'required|numeric'
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $sql = "DELETE
                    FROM mdl_activities_topics
                    WHERE __CONDITIONS__";

        $conditions = array();
        $sqlArguments = array();
        foreach ($arguments['topics'] as $key => $value) {
            array_push($sqlArguments, $value, $userId, $courseId, $arguments['unit']);
            array_push($conditions, "(topic   = ?
                                    AND userid   = ?
                                    AND courseid = ?
                                    AND unit     = ?)");
        }

        $conditionsSQL = join(' OR ', $conditions);
        $sql = str_replace('__CONDITIONS__', $conditionsSQL, $sql);

        try{
            $this->DB->Execute($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while deleting topics", "10x000");
        }

        return true;
    }
}

?>