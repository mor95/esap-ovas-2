<?php

trait Concepts{

    /**
     * Este método se encarga de obtener los conceptos asociados al usuario y al OVA
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *unit*.
     *
     * @api
     *
     * @throws CustomException __5x000__, si ocurre un error al obtener los conceptos asociados a la información provista
     * @throws CustomException __5x001__, si no existen conceptos asociados
     * @throws CustomException __5x002__, si no se encuentra el curso
     *
     * @return array
     */

    protected function getLOConcepts(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'unit' => 'required|numeric',
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        if (is_string($userId) && is_string($courseId)) {
            $sql = "SELECT concept
                      FROM mdl_activities_concepts
                     WHERE userid   = ?
                       AND courseid = ?
                       AND unit     = ?";

            try{
                $concepts = $this->DB->get_records_sql($sql, array(
                    $userId,
                    $courseId,
                    $arguments["unit"],
                ));
            }
            catch (Exception $e) {
                throw new CustomException("There was an error while getting concepts. {$e->getMessage()}", '5x000');
            }

            if (is_array($concepts)) {
                return $concepts;
            } 

            throw new CustomException("No concepts were found for user {$userId} in course {$arguments['shortName']} with ID {$courseId}", "5x001");

        }

        throw new CustomException("Course wasn't found.", "5x002");
    }

    /**
     * Este método se encarga de adicionar conceptos en el usuario y OVA provistos
     * @param array $arguments  Un arreglo asociativo
     *
     * @throws CustomException __6x000__ si ocurre un error en la introducción de datos
     * @api
     *
     * @return bool
     */

    protected function addLOConcepts(array $arguments = array()){
        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $this->deleteLOConcepts($arguments);

        $sql = "INSERT INTO mdl_activities_concepts
                          (concept, userid, courseid, unit)
                   VALUES __VALUES__";

        $values = array();
        $sqlArguments = array();
        foreach ($arguments['concepts'] as $key => $value) {
            array_push($sqlArguments, $value, $userId, $courseId, $arguments['unit']);
            array_push($values, "(?, ?, ?, ?)");
        }

        $valuesSQL = join(', ', $values);
        $sql = str_replace('__VALUES__', $valuesSQL, $sql);

        try{
            $this->DB->Execute($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while adding concepts.", "6x000");
        }

        return true;
    }

    /**
     * Este método se encarga de eliminar conceptos asociados a un OVA y usuario
     * @param array $arguments  Un arreglo asociativo que debe contener las propiedades *concepts* y *unit*.
     *
     * @api
     *
     * @throws CustomException __7x000__, si ocurre un error en la eliminación de datos.
     *
     * @return bool
     */

    protected function deleteLOConcepts(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'concepts' => 'required|array',
            'unit' => 'required|numeric'
        ));

        $userId = $this->getUserId($arguments);
        $courseId = $this->getCourseIdByShortName($arguments);

        $sql = "DELETE
                  FROM mdl_activities_concepts
                 WHERE __CONDITIONS__";

        $conditions = array();
        $sqlArguments = array();
        foreach ($arguments['concepts'] as $key => $value) {
            array_push($sqlArguments, $value, $userId, $courseId, $arguments['unit']);
            array_push($conditions, "(concept = ?
                                 AND userid   = ?
                                 AND courseid = ?
                                 AND unit     = ?)");
        }

        $conditionsSQL = join(' OR ', $conditions);
        $sql = str_replace('__CONDITIONS__', $conditionsSQL, $sql);
        try{
            $this->DB->Execute($sql, $sqlArguments);
        }
        catch(Exception $e){
            throw new CustomException("There was an error while deleting concepts.", "7x000");
        }

        return true;
    }
}

?>