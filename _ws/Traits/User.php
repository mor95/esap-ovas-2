<?php

trait User{
    /**
     * Este método se encarga de obtener el ID del usuario en base a la clave de sesión de Moodle
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *sessionId*.
     *
     * @api
     *
     * @throws CustomException __3x000__, si ocurre un error al obtener el ID del usuario a través de la consulta.
     * @throws CustomException __3x001__, si el usuario no existe.
     *
     * @return array
     */

    protected function getUserId(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'sessionId' => 'required|string',
        ));

        $sql = "SELECT userid
                  FROM mdl_sessions
                 WHERE sid = ?";

        try {
            $userId = $this->DB->get_record_sql($sql, array(
                $arguments['sessionId'],
            ));

        } catch (Exception $e) {
            throw new CustomException("There was an error while getting user\'s id. {$e->getMessage()}", '3x000');
        }

        if($userId === false){
            throw new CustomException("This user does not appear to exist. Session key might be wrong", "3x001");
        }

        return $userId->userid;
    }
}

?>