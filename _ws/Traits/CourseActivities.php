<?php

    /**
     * Este método se encarga de obtener todas las actividades dentro del curso (foros, tareas, quizzes, etc.) en base al ID de curso
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedades *courseId*
     *
     * @api
     * 
     * @return array
     */

    trait CourseActivities{
        protected function getCourseActivities(array $arguments = array()){
            $this->validateArguments($arguments, array(
                'unit' => 'required|numeric'
            ));
            $courseId = $this->getCourseIdByShortName($arguments);
            $CFG = $this->CFG;
            require '../course/lib.php';
            $activities = get_array_of_activities($courseId);
            $filteredActivities = [];//Los OVAs se encuentran listados por pestañas en el servidor de la ESAP. La unidad corresponde al ID de pestaña
            foreach ($activities as $key => $value) {
                if($value->section == $arguments['unit']){
                    $filteredActivities[$key] = $value;
                }
            }
            return $filteredActivities;
        }
    }

?>