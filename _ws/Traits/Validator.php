<?php

trait Validator{

    /**
     * Validador de parámetros en métodos.
     * @var class
     */
    private $validator;


    public function __construct(){
        //$this->addValidators();
    }

    /**
     * Este método adiciona validadores a la clase GUMP
     *
     * @api
     */

    protected function addValidators(){
        GUMP::add_validator("string", function($field, $input, $param = NULL) {
            if(!isset($input[$field]) || !is_string($input[$field])){
                throw new Exception("Property '{$field}' is expected to be a string");
            }
            return array(
                'field' => $field,
                'input' => $input,
                'param' => $param
            );
        });

        GUMP::add_validator("array", function($field, $input, $param = NULL) {
            if(!isset($input[$field]) || !is_array($input[$field])){
                throw new Exception("Property '{$field}' is expected to be an array");
            }
            return array(
                'field' => $field,
                'input' => $input,
                'param' => $param
            );
        });
    }

    /**
     * Este método valida el formato pasado a los argumentos.
     *
     * @api
     * @param array $arguments Un arreglo que puede contener uno o varios argumentos
     * @param array $validationArgs Un arreglo que contiene las reglas de validación de argumentos
     *
     * @throws CustomException __4x000__, si se produce un error en la ejecución de la validación
     * @throws CustomException __4x001__, si los argumentos no coinciden con el tipo requerido
     */

    protected function validateArguments(array $arguments = array(), array $validationArgs = array()){
        $this->validator->validation_rules($validationArgs);
        try{
            $validArgs = $this->validator->run($arguments);
        }
        catch(Exception $e){
            throw new CustomException($e->getMessage(), "4x000");
        }

        if ($validArgs === false) {
            $errors = array_values($this->validator->get_errors_array());
            throw new CustomException("Wrong arguments: " . $errors[0], "4x001");
        }
    }
}

?>