<?php
trait Course{
        /**
     * Este método se encarga de obtener el ID del curso en base a su nombre corto
     * @param array $arguments  Un arreglo asociativo que debe contener la propiedad *shortName*.
     *
     * @api
     *
     * @throws CustomException __4x000__, si ocurre un error al obtener el ID del curso en la consulta.
     * @throws CustomException __4x001__, si el curso no existe.
     *
     * @return array
     */

    protected function getCourseIdByShortName(array $arguments = array()){
        $this->validateArguments($arguments, array(
            'shortName' => 'required|string',
        ));

        $sql = "SELECT id
                  FROM mdl_course
                 WHERE shortname = ?";

        try {
            $courseId = $this->DB->get_record_sql($sql, array(
                $arguments['shortName'],
            ));

        } catch (Exception $e) {
            throw new CustomException("There was an error while getting course's id. {$e->getMessage()}", '4x000');
        }

        if($courseId === false){
            throw new CustomException("This course does not appear to exist. Short name might be wrong", "4x001");
        }

        return $courseId->id;
    }
}


?>