'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const ovaProgress = require("ova-progress");
const ovaConcepts = require("ova-concepts");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    currentProps: {
        seenSlides: {},
        finished: false
    },
    addEventListeners: function () {
        var self = this;
        $('.cont-button-item li').on("click", function(){
            $(this).addClass("see-item").removeClass("tada");
            $(".cont-item").addClass("animated bounceInUp item-disabled");
            var datali = $(this).data("li-item");
            var datalinext = datali+1;
            $('.cont-button-item li[data-li-item="'+ datalinext + '"]').addClass("animated tada");
            $('.cont-item[data-cont-item="'+ datali + '"]').removeClass("item-disabled");
            module.exports.currentProps.seenSlides[datali] = true;
            module.exports.checkModals();
            
        })
    },
    checkModals: function () {
        if (module.exports.currentProps.finished == true) {
            return true;
        }
        if (_.size(module.exports.currentProps.seenSlides) == 8) {
            var self = this;
            console.log(this)
            $(".button-continuar").css("display" , "block");
            $(".button-continuar").on("click" , function(){
                var targetConcepts = _.pick(concepts, [
                    "Acreditación en salud",
                    "Calidad",
                    "Dispositivo médico"
                ]);
                ovaConcepts.showNewConceptsModal({
                    title: 'Observe los siguientes conceptos e intente recordar su definición.',
                    subtitle: 'Después acceda al cofre de conceptos y compare si su definición se ajusta a la que aquí se propone',
                    concepts: targetConcepts
                }).then(function (value) {
                    presentation.switchToSlide({
                        slide: $('.slide[data-slide="1"]')
                    });
                    ovaConcepts.updateConcepts({
                        concepts: targetConcepts,
                        action: "insert",
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'Recursos 1': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    })
                })
            })

        }
    }
}


