'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const ovaProgress = require("ova-progress");
const ovaConcepts = require("ova-concepts");
const _ = require("lodash");
const courseData = require('courseData.json');
module.exports = {
    currentProps: {
        finished: false
    },
    addEventListeners: function () {
        var self = this;
        // console.log(this)
        // if(_.size(self.finishedTopics) == 5){
        //     swal.close();
        // }
        if (_.isObject(self.finishedTopics['Actividad 1'])) {
            // CHANGE HOME 1
            $('.home-exp[data-exp="1"]').addClass("animated pulse");
            $(".button-exp").removeClass("disabled");
            $(".initial-message").css("display", "none");
            $('.button-act[data-button-act="1"]').removeClass("animate-active").addClass("disabled");
        }
        // if (_.isObject(self.finishedTopics['Recursos 1'])) {
        //     // CHANGE HOME 2
        //
        // }
        //
        // $('.home-exp[data-exp="1"]').on("click", function(){
        //     var targetConcepts = _.pick(concepts, [
        //         "Concepto uno",
        //         "Concepto dos"
        //     ]);
        //     ovaConcepts.showNewConceptsModal({
        //         concepts: targetConcepts,
        //         title: 'Observe los conceptos e intente recordar su definición:',
        //         subtitle: 'Después acceda al cofre de conceptos y compare si su definición se ajusta a la que aquí se propone.'
        //
        //
        //     }).then(function (value) {
        //         presentation.switchToSlide({
        //             slide: $('.slide[data-slide="1"]')
        //         });
        //         ovaConcepts.updateConcepts({
        //             concepts: targetConcepts,
        //             action: "insert",
        //             courseShortName: courseData.shortName,
        //             courseUnit: courseData.unit
        //         });
        //         self.ovaProgress.updateFinishedTopics({
        //             topics: {
        //                 'Recursos 2': true
        //             },
        //             action: 'add',
        //             courseShortName: courseData.shortName,
        //             courseUnit: courseData.unit
        //         })
        //     })
        //
        // })
        //
        // $('.home-exp[data-exp="2"]').on("click", function(){
        //     swal({
        //         target: stageResize.currentProps.$stage.get(0),
        //         customClass: 'ova-send-activity',
        //         showCloseButton: true,
        //         showConfirmButton: false,
        //         html: require("templates/modal_act_1.hbs")({
        //           assignLink: _.get(self, 'courseActivities.assign.0.href')
        //         })
        //       })
        // })
    }
}
