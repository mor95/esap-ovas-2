<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PLANEACIÓN ESTRATÉGICA</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<style>
    img {
        width: 100%;
    }

    @font-face {
        font-family: 'BebasNeue';
        src: url('assets/fonts/BebasNeue.ttf');
    }

    @font-face {
        font-family: 'FFDINPro-Bold';
        src: url('assets/fonts/FFDINPro-Bold.ttf');
    }

    @font-face {
        font-family: 'FFDINPro-Light';
        src: url('assets/fonts/FFDINPro-Light.ttf');
    }

    @font-face {
        font-family: 'FFDINPro-Thin';
        src: url('assets/fonts/FFDINPro-Thin.ttf');
    }

    @font-face {
        font-family: 'FFDINProCond-Bold';
        src: url('assets/fonts/FFDINProCond-Bold.ttf');
    }

    @font-face {
        font-family: 'FFDINProCond-Light';
        src: url('assets/fonts/FFDINProCond-Light.ttf');
    }

    @font-face {
        font-family: 'FFDINProCond-Regular';
        src: url('assets/fonts/FFDINProCond-Regular.ttf');
    }

    @font-face {
        font-family: 'MyriadPro-Regular';
        src: url('assets/fonts/MyriadPro-Regular.ttf');
    }

    @font-face {
        font-family: 'wingding';
        src: url('assets/fonts/wingding.ttf');
    }

    @font-face {
        font-family: 'Wingdings-Regular';
        src: url('assets/fonts/Wingdings-Regular.ttf');
    }

    html {

    }

    body {
        font-size: 12px;
        font-family: 'MyriadPro-Regular';
    }

    h2 {
        font-family: 'FFDINProCond-Bold';
        font-size: 20px;
        font-weight: 100;
        text-align: center;
        color: #04371c;
    }

    p {
        font-size: 20px;
        color: #0a7e3f;
        font-family: 'MyriadPro-Regular';
    }

    @page {
        margin: 100px 25px;
    }

    header {
        position: fixed;
        top: -60px;
        left: 0px;
        right: 0px;
        height: 50px;
        background-color: #0a7e3f;
        padding: 10px;
    }

    footer {
        position: fixed;
        bottom: -60px;
        left: 0px;
        right: 0px;
        font-size: 12px;
        font-family: 'FFDINProCond-Bold';
        text-align: center;
        background-color: #0a7e3f;
        color: #FFFFFF;
        padding: 10px;
    }

    header h1 {
        font-family: 'FFDINProCond-Bold';
        font-size: 20px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #04371c;
        padding-left: 80px;
    }

    header h2 {
        font-family: 'FFDINProCond-Bold';
        font-size: 16px;
        font-weight: 100;
        margin: 0;
        padding: 0;
        text-align: left;
        color: #FFFFFF;
        padding-left: 80px;
    }

    #esap-logo {
        position: absolute;
        width: 60px;
    }

    .question-text {
        font-family: 'FFDINPro-Light';
        color: #04371c;
        font-size: 16px;
    }

    .date {
        font-family: 'FFDINPro-Light';
        color: #545454;
        text-align: right;
        font-size: 8px;
    }

    .date p {
        font-size: 12px;
    }

    .date b {
        font-family: 'FFDINPro-Bold';
        font-weight: inherit;
    }

    .quiz-title {
        font-family: 'FFDINPro-Light';
        margin: 0 18px 0 10px;
        padding: 0;
        font-size: 24px;
    }

    .contRtaUno {
        width: 100%;
        height: auto;
    }

    .contRtaUno h2 {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
    }
    .contRtaUno p {
        padding: 0%;
        margin: 0%;
        font-size: 2em;
        text-align: center;
    }
    .contRtaUno p span{
        font-family: 'FFDINPro-Bold';
    }





</style>

<body>
    <header>
        <img src="assets/img/esap_logo.png" id="esap-logo">
        <h1>PLANEACIÓN ESTRATÉGICA</h1>
        <h2>UD4 RESPONSABILIDAD SOCIAL EMPRESARIAL - RSE</h2>
    </header>
    <hr>

    <div class="date">
        <p>Hora de generación:
            <b>{$date}</b>
        </p>
    </div>

    <h2 class="quiz-title">Respuestas Actividad 3</h2>
    <br>
    <div class="contRtaUno">
        <h2>¿Identifica los conceptos básicos, el alcance y las actividades que implica el proceso de RSE contenida en las normas de certificación y auditoría que puede ser empleados por una empresa de cualquier sector de la economía?</h2>
        <p> <span>{$r1} </span> los conceptos básicos y el proceso de RSE que puede ser aplicado en una empresa.</p>
        <br>
        <br>
        <br>
        <h2>¿Reconoce la importancia y alcance de la RSE como herramienta de gestión contenida en las normas de certificación y auditoría?</h2>
        <p> <span>{$r2} </span> la importancia y alcance de la RSE como herramienta de gestión contenida en las normas de certificación y auditoría.</p>
        <br>
    </div>

    <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>

</body>

</html>
