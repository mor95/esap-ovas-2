const _ = require("lodash");
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const courseData = require('courseData.json');
var $textarray= new Array()
var $textarrayf;
module.exports = {
  currentProps: {
      $textarrayf:0
  },
  onEnter:function(){
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed biggest modal_s5',
        showCloseButton: true,
        showConfirmButton: false,
        html: require("templates/modal_s2_1.hbs")()
     })
  },
    addEventListeners: function () {
        var self = this;
        var $dataicon
        $('.btn-cont-item').on('click', function() {
          $('.cont-pregunta').css('display', 'none');
        })
        $('.icon-pregunta').on("click", function(){
          $dataicon = $(this).data('icon-pregunta')
          $('.cont-pregunta[data-pregunta="'+$dataicon+'"]').css('display', 'block');
        })
        $('.button-finish-s2').on("click", function(){
          $('.cont-pregunta .cont-text textarea').each(function(){
            var textoarea = $(this).val();
            console.log(textoarea);
            $textarray.push(textoarea);
            console.log($textarray);
            self.currentProps.$textarrayf = $textarray;
            console.log(self.currentProps.$textarrayf);
            var $textarrayenvio = $textarray;
          })
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: $textarray
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.0.href')
                })
              })
              .then(function (value) {
                console.log(self);
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $(".initial-message").css("display", "none");
                $('.act-ini').addClass("disabled");
                $('.cont-exp , .resource-uno').removeClass('disabled');
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
                require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                  component: 'activities',
                  method: 'addLOActivities',
                  showFeedback: true,
                  arguments: {
                      shortName: require('courseData.json').shortName,
                      unit: require('courseData.json').unit,
                      activityId: 'AutoevaluacionPrimeraUd4',
                      answers: JSON.stringify({
                          textos: {
                            r10: $textarrayenvio
                          }
                      })
                  }
              })
              .then(function(response){

                  console.log('response', response)
              })
            })
        })
    }
}
