'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
var cadatexto = "";
  module.exports = {
    addEventListeners: function () {
        var self = this;
        var $textcinco= new Array(2)

        $('.modal-close').on('click' ,  function(){
          $('.modal_cinco').css('display', 'none');
        })
        $('.item-modal-dos').on('click' ,  function(){
          var $dataitem = $(this).data('item-cinco');
          $('.modal_cinco[data-modal-cinco="'+ $dataitem +'"]').css('display', 'block');
        })


        $('.item-modal').on('click', function(){
          var $dataitemcinco = $(this).data("item-cinco");
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-themed biggest modal_s5',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_s5_" + $dataitemcinco + ".hbs")()
           })
        });
        $('.btn_end_acttwon').on("click", function(){
          $('.modal_cinco').css('display', 'none');
          var $textouno =$('.textoUno textarea').val();
          $textcinco[0]= $textouno;
          var $textodos =$('.textoDos textarea').val();
          $textcinco[1]= $textodos;
          console.log('texto', $textcinco);
          swal({
              target: stageResize.currentProps.$stage.get(0),
              customClass: 'ova-send-activity',
              showCloseButton: true,
              showConfirmButton: false,
              html: require("templates/modal_act_2.hbs")({
                parametros: $.param({
                  r1: $textcinco
                }),
                assignLink: _.get(self, 'courseActivities.forum.0.href')
              })
            })
            .then(function (value) {
              console.log(self);
              $textcinco.length=0;
              self.ovaProgress.updateFinishedTopics({
                  topics: {
                      'Actividad 2': true
                  },
                  action: 'add',
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              })
              presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
              });
          })
    });

}
}
