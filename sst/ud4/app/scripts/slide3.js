'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    addEventListeners: function () {
        var self = this;
        $('.btn_cont_S3').on("click", function(){
          var targetConcepts = _.pick(concepts, [
          "Bienestar integral",
          "Condiciones y medio ambiente de trabajo",
          "Gestión del talento humano",
          "Gestión organizacional",
          "Apoyo organizacional"
          ]);
          self.ovaConcepts.showNewConceptsModal({
              concepts: targetConcepts,
              title: '¿En su contexto laboral ha aplicado estos conceptos?',
              subtitle: 'Imagine una situación cotidiana donde se usen estos conceptos. Lea en el cofre las definiciones de estos y compare sus imágenes con la información que allí encontró.'

          }).then(function (value) {
              presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
              });
              self.ovaConcepts.updateConcepts({
                  concepts: targetConcepts,
                  action: "insert",
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              });
              self.ovaProgress.updateFinishedTopics({
                  topics: {
                      'Recursos 1': true
                  },
                  action: 'add',
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              })
          })
        })
    }
}
