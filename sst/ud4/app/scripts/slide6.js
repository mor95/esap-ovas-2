'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    currentProps:{
      seenSlides: {},
      finished: false
  },
  onEnter: function(){
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed modal_s6',
        showCloseButton: false,
        showConfirmButton: false,
        html: require("templates/modal_s6_1.hbs")()
     })
     $('.ini-s6').on('click', function() {
       swal.close();
     })
  },
    addEventListeners: function () {
        var self = this;
        var audioUno = document.getElementById('soundUno');
        // $('.disc audio').on('play', function(){
        //   var $dataaudio = $(this).parent().data("disc");
        //   console.log($dataaudio);
        //   this.onended = function () {

        //   }
        //
        // })
        $('.element').on('click', function(){
          $('.element').removeClass('element-active');
          $(this).addClass('element-active');
          var dataelement = $(this).data('element');
          $('.contAudio audio').attr('src', 'img/sound/sound_'+ dataelement +'.mp3');
          $('.cont-screen').css('background-image', 'url(img/6/element-' + dataelement+ '.png)');
          audioUno.play();
          // setTimeout(function(){
          //   $('.contAudio').addClass('animated bounceInDown').css('display', 'block');
          // },500)
            module.exports.currentProps.seenSlides[dataelement]= true;
            module.exports.checkModals();
         })
        $('.btn_cont_S6').on('click' , function(){
          presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
          });
          self.ovaProgress.updateFinishedTopics({
              topics: {
                  'Recursos 3': true
              },
              action: 'add',
              courseShortName: courseData.shortName,
              courseUnit: courseData.unit
          })
        })
    },
    checkModals: function () {
        var self = this;
        if (module.exports.currentProps.finished == true) {
            return true;
        }
        if (_.size(module.exports.currentProps.seenSlides) == 9) {
          $('.btn_cont_S6').css('visibility' , 'visible');
        }
    }
}
