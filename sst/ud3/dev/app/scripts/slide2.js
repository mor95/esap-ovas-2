const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

    currentProps: {
        oldAnswers: []
    },

	onEnter: function(){
		swal({
		    target: stageResize.currentProps.$stage.get(0),
		    allowOutsideClick: false,
		    allowEscapeKey: false,
		    customClass: 'ova-themed modalText',
		    showCloseButton: true,
		    showConfirmButton: false,
		    title: 'Teniendo en cuenta sus conocimientos responda las siguientes preguntas.',
		});
	},

    addEventListeners: function(){

    	var $pregunta;

    	var self = this;

        var initialAnswers = [];

        $('.slide[data-slide="2"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            initialAnswers[$respuesta-1] = $this.val();
        });


        $("[data-slide='2'] .boton-pregunta").on("click", function () {
        	$this = $(this);
        	$pregunta = $this.data("boton-pregunta");
        	$this.addClass("activo");

            $("[data-slide='2'] .pagina").addClass("oculto");
            $("[data-slide='2'] .pagina[data-pagina='" + ($pregunta+1) + "']").removeClass("oculto");
            $("[data-slide='2'] .acciones").removeClass("oculto");
        });

        $("[data-slide='2'] .volver").on("click", function () {
        	vuelve();
        });

        $("[data-slide='2'] .guardar").on("click", function () {
            module.exports.currentProps.oldAnswers = initialAnswers;
            console.log(module.exports.currentProps.oldAnswers);
            $("[data-slide='2'] .pagina[data-pagina='" + ($pregunta+1) + "']").addClass("visto");
        	vuelve();
        });

        function vuelve(){ 
            $("[data-slide='2'] .pagina").addClass("oculto");
            $("[data-slide='2'] .acciones").addClass("oculto");
            $("[data-slide='2'] .pagina[data-pagina='1']").removeClass("oculto");
            validaFinaliza();
        }

        function validaFinaliza(){
        	if ($("[data-slide='2'] .visto").length == $("[data-slide='2'] .paginita").length) {
                subirRespuestas();
        	}
        }

        // $("[data-slide='2'] .button-continuar").on("click", function () {

        function subirRespuestas(){

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                component: 'activities',
                method: 'addLOActivities',
                showFeedback: true,
                arguments: {
                    shortName: require('courseData.json').shortName,
                    unit: require('courseData.json').unit,
                    activityId: 'Autoevaluacion primera',
                    answers: JSON.stringify({
                        respuestas: module.exports.currentProps.oldAnswers
                    })
                }
            })
            .then(function(response){
                console.log('response', response);
            })

            cargarRespuestasIniciales();

            console.log(module.exports.currentProps.oldAnswers);

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.oldAnswers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.oldAnswers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });
            console.log("link " + _.get(self, 'courseActivities.assign.0.href'));
        	swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide2Pdf.hbs")( 
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.0.href')
                        } 
                    ),
            }).then(function (t) { 
                $("[data-slide='2'] .button-continuar").removeClass("escondido");
            });
        }

        $("[data-slide='2'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'evaluacion_inicial': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .edificio').removeClass("disabled");
                $('.slide[data-slide="1"] .recurso').removeClass("disabled");
                $('.slide[data-slide="1"] .actividad').removeClass("disabled");
                $('.slide[data-slide="3"]').data("presentation").enabled = true;
            }, 2500);
            setTimeout(function () {
                $('.slide[data-slide="2"]').data("presentation").enabled = false;
                $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");
            }, 3500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .recurso[data-recurso="1"]').addClass("click-me");
            }, 4500);
        });

        function cargarRespuestasIniciales(){
            for (var i = 0; i < module.exports.currentProps.oldAnswers.length; i++) {
                $('.slide[data-slide="5"]  textarea[data-respuesta="' + (i+1) + '"]').text(module.exports.currentProps.oldAnswers[i]);
            }
        }
	}
}