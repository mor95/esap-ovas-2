const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const concepts = require("concepts.json");
const YouTubePlayer = require('youtube-player');

module.exports = {

    addEventListeners: function(){

        if (!_.isObject(this.finishedTopics['evaluacion_inicial'])) {
            setTimeout(function () {
                   swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed allDoneModal',
                showCloseButton: true,
                showConfirmButton: false,
                title: 'En la presente unidad usted aprenderá a desarrollar estrategias de fortalecimiento organizacional para la promoción y vigilancia del sistema de gestión a través de planes de intervención, desde el Comité Paritario de Seguridad y Salud en el Trabajo conocido como el Copasst.<br><br>Para empezar inicie la Actividad de aprendizaje 1 de la primera Experiencia de Aprendizaje.',
            });
               }, 4000);   
        }

        var self = this;

        console.log("this", self);
        console.log(self.activities);

        $("#presentation").on("slide", function (e, i) {
            console.log("onSlide");
            $('.navigation-buttons .home').addClass("oculto");
            $('.navigation-buttons .next').addClass("oculto");
            $('.navigation-buttons .prev').addClass("oculto");

            var slideId = presentation.currentProps.$slide.attr("data-slide");
            console.log(slideId);
            if (slideId != 1) {
                setTimeout(function () {
                    $('.navigation-buttons .home').removeClass("oculto");
                    //$('.navigation-buttons .home').addClass("volver");
                }, 1000);
            }

            if (slideId == 5) {
                $('.navigation-buttons .next').removeClass("oculto");
                setTimeout(function () {
                    $('.navigation-buttons .next').addClass("highlight");
                }, 1000);
            }

            // if (slideId == 3 || slideId == 5 || slideId == 8 || slideId == 11) { 
            //     $('.navigation-buttons .prev').removeClass("oculto");
            // }
        });

        $("[data-slide='1'] .recurso[data-recurso='1']").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video.hbs")(self.media),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Condiciones y medio ambiente de trabajo",
                        "Accidente de Trabajo (AT)",
                        "Riesgo",
                        "Enfermedad laboral",
                        "Riesgo aceptable"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Ha escuchado antes estas palabras?",
                        subtitle: "Imagine una situación cotidiana donde se usen estos conceptos. Lea en el cofre las definiciones de estos y compare sus imágenes con la información que allí encontró.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1500);
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_1': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 3000);
                        setTimeout(function () {
                            $('.slide[data-slide="1"] .recurso[data-recurso="2"]').addClass("click-me");
                        }, 4500);
                    })
                });
            }
        });

        $("[data-slide='1'] .recurso[data-recurso='2']").on("click", function () {
            if (!$(this).hasClass("disabled")) {
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed video interactivo',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide1Video2.hbs")(),
                }).then(function (t) {
                    var targetConcepts = _.pick(concepts, [
                        "Análisis del riesgo",
                        "Medida de control",
                        "Rendición de cuentas",
                        "Requisito Normativo",
                        "Comité Paritario de Seguridad y Salud en el Trabajo"
                    ]);
                    
                    self.ovaConcepts.showNewConceptsModal({
                        title: "¿Relaciona estos términos con algún proceso en su contexto laboral?",
                        subtitle: "Imagine una situación cotidiana donde se usen estos conceptos. Lea en el cofre las definiciones de estos y compare sus imágenes con la información que allí encontró.",
                        concepts: targetConcepts
                    })
                    .then(function (t) {
                        setTimeout(function () {
                            self.ovaConcepts.updateConcepts({
                                concepts: targetConcepts,
                                action: "insert",
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 1500);
                        setTimeout(function () {
                            self.ovaProgress.updateFinishedTopics({
                                topics: {
                                    'recurso_2': true
                                },
                                action: 'add',
                                courseShortName: courseData.shortName,
                                courseUnit: courseData.unit
                            });
                        }, 3000);
                        setTimeout(function () {
                            $('.slide[data-slide="1"] .actividad[data-actividad="2"]').addClass("click-me");
                        }, 4500);
                    })
                });

                var idVideo = self.media.AV.mediaid;
                const player = new YouTubePlayer('videoInteractivo', {
                    videoId: idVideo,
                    playerVars: {
                        rel: 0
                    }
                })
                player.setVolume(100);
                player.stopVideo();
                //const pauseCont = [48, 96, 142.5]

                player.on('stateChange', function () {
                    var stateA = player.getPlayerState();
                    stateA.then(function (value) {
                        console.log(stateA)
                        if (value == 1) {
                            console.log("esta playing")
                            $(".interaccion[data-interaccion='2']").removeClass("oculto");
                            evaluacion()
                        }
                    })
                });

                function evaluacion() {
                    var videoCurrentTime
                    var setInter = setInterval(function () {
                        var time = player.getCurrentTime();

                        time.then(function (valueTime) {
                            //videoCurrentTime = parseInt(valueTime);
                            videoCurrentTime = valueTime;
                            console.log("time", videoCurrentTime);
                        })

                        var state = player.getPlayerState();
                        state.then(function (valueK) {
                            if (valueK == 2) {
                                console.log("esta paused")
                                clearInterval(setInter);
                            }

                            if (valueK == 0) {
                                console.log("termina")
                                clearInterval(setInter);
                                $(".interaccion").addClass("oculto");
                            }
                        })

                        //MENU
                        if ((videoCurrentTime >= 25 && videoCurrentTime <= 25.9)) {
                            player.pauseVideo();
                             $(".interaccion").addClass("oculto");
                             $(".interaccion[data-interaccion='1']").removeClass("oculto");
                             // $(".interaccion .continuar").removeClass("clicked");
                        }
  
                        if (videoCurrentTime >= 52 && videoCurrentTime <= 52.5 || videoCurrentTime >= 76 && videoCurrentTime <= 76.5 || videoCurrentTime >= 108 && videoCurrentTime <= 108.5 || videoCurrentTime >= 170 && videoCurrentTime <= 170.5 || videoCurrentTime >= 223 && videoCurrentTime <= 223.5) {
                            if($(".interactivo .visto").length == $(".interactivo .boton").length){
                                 player.seekTo(224) 
                                 player.playVideo();
                            }
                            else{   
                                player.seekTo(25)
                                player.pauseVideo();
                                $(".interaccion[data-interaccion='1']").removeClass("oculto");
                                $(".interaccion[data-interaccion='2']").addClass("oculto");
                            }
                        }
                    }, 500)
                }
                $(".interactivo .boton").on("click", function () {
                    $(".interaccion[data-interaccion='1']").addClass("oculto");
                    $(".interaccion[data-interaccion='2']").removeClass("oculto");
                    opcion = $(this).data("boton");
                    $(this).addClass("visto");
                    if (opcion == 1) 
                        player.seekTo(26.5)
                    if (opcion == 2) 
                        player.seekTo(56)
                    if (opcion == 3) 
                        player.seekTo(80.3)
                    if (opcion == 4) 
                        player.seekTo(112.3)
                    if (opcion == 5) 
                        player.seekTo(174)
                    player.playVideo()
                });
            }
        });

        // $("[data-slide='1'] .estrella[data-estrella='4']").on("click", function () {
        //     if(!$(this).hasClass("disabled")){
        //         swal({
        //             target: stageResize.currentProps.$stage.get(0),
        //             allowOutsideClick: false,
        //             allowEscapeKey: false,
        //             customClass: 'ova-themed video',
        //             showCloseButton: true,
        //             showConfirmButton: false,
        //             html: require("templates/slide1Video.hbs")(self.media),
        //         }).then(function (t) {
        //             var targetConcepts = _.pick(concepts, [
        //                 "concepto 7"
        //             ]);
                    
        //             ovaConcepts.showNewConceptsModal({
        //                 title: "Conteste mentalmente la siguiente pregunta",
        //                 subtitle: "¿Considera usted que estos estándares se cumplen en su IPS más cercana?",
        //                 concepts: targetConcepts
        //             })
        //             .then(function (t) {
        //                 setTimeout(function () {
        //                     ovaConcepts.updateConcepts({
        //                         concepts: targetConcepts,
        //                         action: "insert",
        //                         courseShortName: courseData.shortName,
        //                         courseUnit: courseData.unit
        //                     });
        //                 }, 500);
        //                 setTimeout(function () {
        //                     self.ovaProgress.updateFinishedTopics({
        //                         topics: {
        //                             'recurso_3': true
        //                         },
        //                         action: 'add',
        //                         courseShortName: courseData.shortName,
        //                         courseUnit: courseData.unit
        //                     });
        //                 }, 1500);
        //                 setTimeout(function () {
        //                     $('.slide[data-slide="1"] .estrella[data-estrella="4"]').addClass("dorada"); 
        //                 }, 2500);
        //             })
        //         });
        //     }
        // });

        // $("[data-slide='1'] .estrella[data-estrella='7']").on("click", function () {
        //     if(!$(this).hasClass("disabled")){
        //         swal({
        //             target: stageResize.currentProps.$stage.get(0),
        //             allowOutsideClick: false,
        //             allowEscapeKey: false,
        //             customClass: 'ova-themed video',
        //             showCloseButton: true,
        //             showConfirmButton: false,
        //             html: require("templates/slide1Video1.hbs")(self.media),
        //         }).then(function (t) {
        //             self.ovaProgress.updateFinishedTopics({
        //                 topics: {
        //                     'recurso_5': true
        //                 },
        //                 action: 'add',
        //                 courseShortName: courseData.shortName,
        //                 courseUnit: courseData.unit
        //             });
        //             setTimeout(function () {
        //                 $('.slide[data-slide="1"] .estrella[data-estrella="7"]').addClass("dorada"); 
        //             }, 1000);
        //         });
        //     }
        // });


        $('.navigation-buttons .next').addClass("oculto");
        $('.navigation-buttons .prev').addClass("oculto");
        $('.navigation-buttons .home').addClass("oculto");

        if (_.isObject(this.finishedTopics['evaluacion_inicial'])) {
            $('.slide[data-slide="1"] .edificio').removeClass("disabled");
            $('.slide[data-slide="1"] .recurso').removeClass("disabled");
            $('.slide[data-slide="1"] .actividad').removeClass("disabled");
            $('.slide[data-slide="3"]').data("presentation").enabled = true;
            $('.slide[data-slide="2"]').data("presentation").enabled = false;
            $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");         
        }

        if (_.isObject(this.finishedTopics['evaluacion_final'])) {
            // $('.slide[data-slide="5"]').data("presentation").enabled = false;
            // $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled");  
        }
    }
}