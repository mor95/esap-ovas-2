const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

    currentProps: {
        newAnswers: []
    },

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed modalText',
            showCloseButton: true,
            showConfirmButton: false,
            title: '¿Recuerda qué contesto en la primera actividad?<br> En este momento puede cambiar o complementar su respuestas, según lo aprendido a lo largo de esta unidad.',
        });
    },

    addEventListeners: function(){

        var $pregunta;

        var self = this;

        var finalAnswers = [];

        $('.slide[data-slide="5"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            finalAnswers[$respuesta-1] = $this.val();
        });


        $("[data-slide='5'] .boton-pregunta").on("click", function () {
            $this = $(this);
            $pregunta = $this.data("boton-pregunta");
            $this.addClass("activo");

            $("[data-slide='5'] .pagina").addClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").removeClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='" + ($pregunta+1) + "']").addClass("visto");
            $("[data-slide='5'] .acciones").removeClass("oculto");
        });

        $("[data-slide='5'] .volver").on("click", function () {
            vuelve();
        });

        $("[data-slide='5'] .guardar").on("click", function () {
            module.exports.currentProps.newAnswers = finalAnswers;
            console.log(module.exports.currentProps.newAnswers);
            vuelve();
        });

        function vuelve(){ 
            $("[data-slide='5'] .pagina").addClass("oculto");
            $("[data-slide='5'] .acciones").addClass("oculto");
            $("[data-slide='5'] .pagina[data-pagina='1']").removeClass("oculto");
            validaFinaliza();
        }

        function validaFinaliza(){
            if ($("[data-slide='5'] .visto").length == $("[data-slide='5'] .paginita").length) {
                $("[data-slide='5'] .button-continuar").removeClass("escondido");
            }
        }

        cargarRespuestasIniciales();

        function cargarRespuestasIniciales(){
            if (_.isObject(self.activities['Autoevaluacion primera'])) {
                respuestasIniciales = self.activities['Autoevaluacion primera'].respuestas;
                console.log(respuestasIniciales);
                for (var i = 0; i < respuestasIniciales.length; i++) {
                    $('.slide[data-slide="5"]  textarea[data-respuesta="' + (i+1) + '"]').text(respuestasIniciales[i]);
                }
                
            }
        }

        $("[data-slide='5'] .button-continuar").on("click", function () {

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
            component: 'activities',
            method: 'addLOActivities',
            showFeedback: true,
            arguments: {
                shortName: require('courseData.json').shortName,
                unit: require('courseData.json').unit,
                activityId: 'Autoevaluacion final',
                answers: JSON.stringify({
                    respuestas: module.exports.currentProps.newAnswers
                })
            }
        })
        .then(function(response){
            console.log('response', response)
        })
            console.log(module.exports.currentProps.newAnswers);

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.newAnswers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.newAnswers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });
            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide5Pdf.hbs")( 
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.2.href')
                        } 
                    ),
            }).then(function (t) {
                presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
                });
                setTimeout(function () {
                    self.ovaProgress.updateFinishedTopics({
                        topics: {
                            'evaluacion_final': true
                        },
                        action: 'add',
                        courseShortName: courseData.shortName,
                        courseUnit: courseData.unit
                    });
                }, 1500);
                setTimeout(function () {
                    $('.slide[data-slide="5"]').data("presentation").enabled = false;
                    $('.slide[data-slide="1"] .actividad[data-actividad="3"]').addClass("disabled");
                }, 30000);
            });
        });
    }
}