<!DOCTYPE html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>SEGURIDAD Y SALUD EN EL TRABAJO (SST)</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <style>
        img {
            width: 100%;
        }
        @font-face {
            font-family: 'BebasNeue';
            src: url('../fonts/BebasNeue.otf');
        }
        @font-face {
            font-family: 'FFLight';
            src: url('../fonts/FF_DIN_Pro_Light.otf');
        }
        @font-face {
            font-family: 'FFBold';
            src: url('../fonts/FF_DIN_Pro_Bold.otf');
        }
        @font-face {
            font-family: 'FFBCondBold';
            src: url('../fonts/FF_DIN_Pro_Cond_Bold.otf');
        }
        @font-face {
            font-family: 'FFCondRegular';
            src: url('../fonts/FF_DIN_Pro_Cond_Regular.otf');
        }
        @font-face {
            font-family: 'FFCondLight';
            src: url('../fonts/FF_DIN_Pro_Cond_Light.otf');
        }
        @font-face {
            font-family: 'FFLight';
            src: url('../fonts/FF_DIN_Pro_Light.otf');
        }
        @font-face {
            font-family: 'FFThin';
            src: url('../fonts/FF_DIN_Pro_Thin.otf');
        }
        @font-face {
            font-family: 'Wingding';
            src: url('../fonts/wingding.ttf');
        }
        @font-face {
            font-family: 'century';
            src: url('../fonts/CenturyGothic.ttf');
        }
        @font-face {
            font-family: 'gothiddd';
            src: url('../fonts/GOTHIC.ttf');

        }
        @font-face {
            font-family: 'myriadRegular';
            src: url('../fonts/MyriadPro-Regular.otf');
        }

        html {

        }

        body {
            font-size: 12px;
            font-family: 'gothiddd';
            padding-top:30px;
        }

        h2 {
            font-family: 'gothiddd';
            font-size: 15px;
            font-weight: 100;
            text-align: center;
            color: #90133C;
        }

        p {
            font-size: 20px;
            color:#90133C;
            font-family: 'gothiddd';
        }

        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 70px;
            background-color: #90133C;
            padding: 10px;
            border-radius: 20px;
            margin-bottom: 3%;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            font-size: 12px;
            font-family: 'FFDINProCond-Bold';
            text-align: center;
            background-color: #90133C;
            color: #FFFFFF;
            padding: 10px;
            border-radius: 10px;
        }

        header h1 {
            font-family: 'gothiddd';
            font-size: 20px;
            font-weight: 100;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #FFF;
            padding-left: 80px;

        }

        header h2 {
            font-family: 'gothiddd';
            font-size: 15px;
            font-weight: 100;
            margin: 0;
            padding: 0;
            text-align: left;
            color: #FFFFFF;
            padding-left: 80px;
        }

        #esap-logo {
            position: absolute;
            width: 60px;
        }

        .question-text {
            font-family: 'FFDINPro-Light';
            color: #4A4A4C;
            font-size: 16px;
        }

        .date {
            font-family: 'FFDINPro-Light';
            color: #90133C;
            text-align: right;
            font-size: 8px;
            border-bottom: solid #90133C 1px dashed;
            margin-bottom: 10px;
        }

        .date p {
            font-size: 12px;
        }

        .date b {
            font-family: 'FFDINPro-Bold';
            font-weight: inherit;
        }

        .quiz-title {
            font-family: 'FFDINPro-Light';
            margin: 0 18px 0 10px;
            padding: 0;
            font-size: 24px;
        }

        .contRtaUno {
            width: 100%;
            height: auto;
        }

        .contRtaUno h2 {
            padding: 0%;
            margin: 0%;
            font-size: 2em;
        }
        .contRtaUno p {
            padding: 0%;
            margin: 0%;
            font-size: 2em;
            text-align: center;
        }
        .contRtaUno p span{
            font-family: 'FFDINPro-Bold';
        }

        .answer-text{
            font-family:'FFDINPro-Light';
            font-size:16px;
            margin-left:16px;
            background-color:#D3D2D6;
            padding:5px;
        }

        .oficina{
            position: relative;
            background-image: url(../img/3/locura.png);
            width: 100%;
            height: 77.506%;
            padding-top: 5%;
            background-size:contain;
            background-repeat:no-repeat;
            border: solid transparen;

        }

        .caja{
            display: inline-block;
              width: 14.506%;
              height: 31.506%;
              border: solid transparen;
        }

        .circulo{
          background-image: url(../img/3/circulo_pdf.png);
          width: 150px;
          height: 100px;
          margin-left: 20%;
          margin-top: 4%;
          background-size:auto;
          background-repeat:no-repeat;
        }
    </style>

    <body>
        <header>
            <img src="../img/esap_logo.png" id="esap-logo">
            <h1>SEGURIDAD Y SALUD EN EL TRABAJO (SST)</h1>
            <h2>PELIGROS, EVALUACIÓN Y VALORACIÓN DE RIESGOS EN EL DESARROLLO ORGANIZACIONAL Y HUMANO DE LAS EMPRESAS</h2>
        </header>
        <hr>

        <div class="date">
            <p>Hora de generación: <b>{$date}</b></p>
        </div>

        <h2 class="quiz-title">Actividad de aprendizaje 2</h2>
        
        {foreach from=$optionsA2 item=$item key=$key}
            <br>
            <div class="question">
                <p class="question-text">{$key+1}. {$item}</p>
                {foreach from=$pdfArgs item=object key=k}
                    {if $k == $key}
                        {if $k == 0}
                            <div class="oficina">
                                {foreach from=$object['answer'] item=icono key=keyIcono}
                                    <div class="caja">  
                                        {if $icono == 1}
                                            <div class="circulo"></div>
                                        {/if}
                                    </div>
                                {/foreach}
                            </div>
                        {else}
                            <p class="answer-text">{$object['answer']}</p>
                        {/if}
                    {/if}                     
                {/foreach}
            </div>  
        {/foreach}

        <footer>ESAP - 2018 © Departamento de Capacitación - Todos los derechos reservados</footer>
        
    </body>
</html>
