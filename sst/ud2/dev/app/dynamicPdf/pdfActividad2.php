<?php
error_reporting(E_ALL);
require 'vendor/autoload.php';
require 'preguntasActividad2.php';

use Dompdf\Dompdf;
// echo '<pre>';
// var_export($_GET);

$smarty = new Smarty();
$smarty->error_reporting = 1;
$smarty->caching = 0;
$smarty->assign($_GET);
$smarty->assign(array(
    'date' => date("Y-m-d h:i:sa"),
    'optionsA2' =>$optionsA2
));
$view = $smarty->fetch('viewActividad2.tpl');

//echo $view;

$dompdf = new Dompdf();
$dompdf->loadHtml($view);
$dompdf->setPaper('A4', 'landscape');
$dompdf->render();
$dompdf->stream("actividad_2.pdf", array("Attachment" => false));
//$dompdf->stream("justificacion_actividad_MASC.pdf", array("Attachment" => true));
exit(0);
?>
