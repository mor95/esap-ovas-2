const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const draggables = require("draggables.json");
const preciseDraggable = require('precise-draggable');
const slidesManager = require('slidesManager.js');
const slide5 = require("slide5.js")

module.exports = {

    currentProps: {
        oldAnswers: []
    },

	onEnter: function(){
		swal({
		    target: stageResize.currentProps.$stage.get(0),
		    allowOutsideClick: false,
		    allowEscapeKey: false,
		    customClass: 'ova-themed modalText',
		    showCloseButton: true,
		    showConfirmButton: false,
		    title: 'Teniendo en cuenta sus conocimientos responda las siguientes preguntas.',
		});
	},

    addEventListeners: function(){

    	var $pregunta;

    	var self = this;

        var initialDrags = [];
        var initialAnswers = [];

        $('.slide[data-slide="2"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            initialAnswers[$respuesta] = $this.val();
        });

        _.forEach(draggables.droppableZones, function (v, k) {
            $('<div class="droppable drop" data-ani-class="wobble"/>')
            .appendTo($('.slide[data-slide="2"] .dropables'))
            .on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $('.slide[data-slide="2"] .dropables').addClass('dropeado');
                        // if(draggableKey !== droppableKey){
                        //     return true;
                        // }
                        //$(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;
                        $(this).append('<div class="logo fs-bg" data-logo="' + $el.data("logo") +'"></div>');
                        $el.remove();

                        console.log($el.data("logo"));
                        initialDrags.push($el.data("logo"));
                        // if(self.$slide.find('.draggable-element').length === 0){
                            
                        // }
                        $("[data-slide='2'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        });

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){

            var $draggableItems = $('.slide[data-slide="2"] .draggable-items'); //toma div 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="logo fs-bg draggable-element" data-logo="' + (k+1) +'" data-ani-class="wobble" data-ani-delay="200"></div>')
                    .appendTo($('.slide[data-slide="2"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='2'] .reiniciar").on("click", function () {
            $("[data-slide='2'] .drop").html("");
            $("[data-slide='2'] .drop").removeClass("dropeado");
            $('.slide[data-slide="2"] .dropables').removeClass('dropeado');

            console.log(initialDrags);
             initialDrags = [];
             console.log(initialDrags);

             console.log(module.exports.currentProps.oldAnswers[0]);
             module.exports.currentProps.oldAnswers[0] = initialDrags;

             cargarDrags();

             $(this).addClass("oculto");
        });

        $("[data-slide='2'] .preguntas").on("click", function () {
        	$("[data-slide='2'] .botonera").removeClass("oculto");
        });

        // $("[data-slide='2'] .boton-pregunta").on("click", function () {
        // 	$this = $(this);
        // 	$pregunta = $this.data("boton-pregunta");
        // 	$("[data-slide='2'] .botonera").find("div").removeClass("activo");
        // 	$this.addClass("activo");

        // 	$("[data-slide='2'] .pregunta").addClass("oculto");
        // 	$("[data-slide='2'] .pregunta[data-pregunta='" + $pregunta + "']").removeClass("oculto");
        // 	$("[data-slide='2'] .responder").removeClass("oculto");
        // });

        $("[data-slide='2'] .boton-pregunta").on("click", function () {
            $this = $(this);
            $pregunta = $this.data("boton-pregunta");
        	$("[data-slide='2'] .pagina").addClass("oculto");
        	$("[data-slide='2'] .pagina[data-pagina='" + ($pregunta+1) + "']").removeClass("oculto");
        	$("[data-slide='2'] .pagina[data-pagina='" + ($pregunta+1) + "']").addClass("visto");
        	$("[data-slide='2'] .acciones").removeClass("oculto");
        });

        $("[data-slide='2'] .volver").on("click", function () {
        	vuelve();
        });

        $("[data-slide='2'] .guardar").on("click", function () {
            module.exports.currentProps.oldAnswers[0] = initialDrags;
            module.exports.currentProps.oldAnswers[1] = initialAnswers[1];
            module.exports.currentProps.oldAnswers[2] = initialAnswers[2];
            module.exports.currentProps.oldAnswers[3] = initialAnswers[3];
            module.exports.currentProps.oldAnswers[4] = initialAnswers[4];
            console.log(module.exports.currentProps.oldAnswers);
        	vuelve();
        });

        function vuelve(){ 
            $("[data-slide='2'] .pagina").addClass("oculto");
            $("[data-slide='2'] .acciones").addClass("oculto");
            $("[data-slide='2'] .pagina[data-pagina='1']").removeClass("oculto");
            validaFinaliza();
        }

        function validaFinaliza(){
        	if ($("[data-slide='2'] .visto").length == $("[data-slide='2'] .paginita").length) {
        	    $("[data-slide='2'] .finalizar").removeClass("oculto");
        	}
        }

        $("[data-slide='2'] .finalizar").on("click", function () {

            require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
            component: 'activities',
            method: 'addLOActivities',
            showFeedback: true,
            arguments: {
                shortName: require('courseData.json').shortName,
                unit: require('courseData.json').unit,
                activityId: 'Autoevaluación primera',
                answers: JSON.stringify({
                    respuestas: module.exports.currentProps.oldAnswers
                })
            }
        })
        .then(function(response){
            console.log('response', response)
        })
            console.log(module.exports.currentProps.oldAnswers);

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.oldAnswers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.oldAnswers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs
            });

            cargarDragsIniciales();

        	swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide2Pdf.hbs")( 
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.0.href')
                        } 
                    ),
            }).then(function (t) {
                $("[data-slide='2'] .button-continuar").removeClass("escondido");
            });
        });

        $("[data-slide='2'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'evaluacion_inicial': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .edificio').removeClass("disabled");
                $('.slide[data-slide="1"] .recurso').removeClass("disabled");
                $('.slide[data-slide="1"] .actividad').removeClass("disabled");
                $('.slide[data-slide="3"]').data("presentation").enabled = true;
            }, 2500);
            setTimeout(function () {
                $('.slide[data-slide="2"]').data("presentation").enabled = false;
                $('.slide[data-slide="1"] .actividad[data-actividad="1"]').addClass("disabled");
            }, 3500);
            setTimeout(function () {
                $('.slide[data-slide="1"] .recurso[data-recurso="1"]').addClass("click-me");
            }, 4500);
        });

        function cargarDragsIniciales(){
            console.log(module.exports.currentProps.oldAnswers);

            respuestasIniciales = module.exports.currentProps.oldAnswers;

            contenedor = $("[data-slide='5'] .draggable-items");
            console.log(respuestasIniciales[0]);
            drags = respuestasIniciales[0];
            for (var i = 0; i < drags.length; i++) {
                $("[data-slide='5'] .logo[data-logo='" + drags[i] + "']").remove();
                $("[data-slide='5'] .drop").append('<div class="logo fs-bg" data-logo="' + drags[i] +'"></div>');
                $('.slide[data-slide="5"] .dropables').addClass('dropeado');
                slide5.currentProps.finalDrags.push(drags[i]);
                $("[data-slide='5'] .reiniciar").removeClass("oculto");
            }
            $("[data-slide='5'] textarea[data-respuesta='1']").text(respuestasIniciales[1]);
            $("[data-slide='5'] textarea[data-respuesta='2']").text(respuestasIniciales[2]);
            $("[data-slide='5'] textarea[data-respuesta='3']").text(respuestasIniciales[3]);  
            $("[data-slide='5'] textarea[data-respuesta='4']").text(respuestasIniciales[4]);   
        }
	}
}