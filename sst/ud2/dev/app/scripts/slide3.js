const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');
const draggables = require("draggablesActividad2.json");
const preciseDraggable = require('precise-draggable')
const slidesManager = require('slidesManager.js')

module.exports = {

    currentProps: {
        answers: [],
        elementos: 18
    },

    onEnter: function(){
        swal({
            target: stageResize.currentProps.$stage.get(0),
            allowOutsideClick: false,
            allowEscapeKey: false,
            customClass: 'ova-themed modal3',
            showCloseButton: false,
            showConfirmButton: false,
            html: require("templates/slide3Modal.hbs")(),
        });

        $('.modal3  .explorar').on("click", function () {
            swal.close();
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
        });

        $('.modal3  .continuar').on("click", function () {
            swal.close();
        });
    },  

    addEventListeners: function(){

        var self = this;
        var offset;
        var xPos;
        var yPos;
        var drags = [];
        var $recurso;
        var answersImagen = [];
        var answers = [];

        $('.slide[data-slide="3"]  textarea').on("keyup", function () {
            $this = $(this);
            $respuesta = $this.data("respuesta");

            answers[$respuesta] = $this.val();
        });

        for (var i = 0; i < module.exports.currentProps.elementos; i++) {
            $('<div class="drop"/ data-drop="' + (i+1) + '">')
            .appendTo($('.slide[data-slide="3"] .dropeable'))
            .on({
                'draggableOver': function () {
                    if (preciseDraggable.currentProps.dragging) {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('over');
                        }
                    }
                },
                'draggableLeave': function () {
                    if (!$(this).data('dropped')) {
                        $(this).removeClass('over');
                    }
                },
                'draggableDrop': function () {
                    preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
                    if (!$(this).data('dropped')) {
                        var element = preciseDraggable.currentProps.lastDraggable;
                        var draggableKey = element.data.key;
                        var droppableKey = $(this).attr('data-key')
                        $(this).removeClass('over');
                        $('.slide[data-slide="3"] .dropables').addClass('dropeado');
                        // if(draggableKey !== droppableKey){
                        //     return true;
                        // }
                        $(this).data('dropped', true);
                        var $el = preciseDraggable.currentProps.lastDraggable.$element;

                        var tipo;

                        $(this).append("<div class='circulo fs-bg'></div>");

                        
                        $el.remove();

                        answersImagen[$(this).data("drop")-1] = 1;

                        //console.log($(this).data("drop"));
                        console.log(answersImagen);

                        $("[data-slide='3'] .reiniciar").removeClass("oculto");
                    }
                    
                }
            });
        }

        var $droppableElements = self.$slide.find('.drop');

        cargarDrags();

        function cargarDrags(){

            var $draggableItems = $('.slide[data-slide="3"] .draggable-items'); //toma div 
            $draggableItems.html("");

            _.forEach(draggables.items, function (v, k) {
                var $el = $('<div class="circulo fs-bg draggable-element" data-circulo="' + k + '"></div>')
                    .appendTo($('.slide[data-slide="3"] .draggable-items'));

                $el.on('mouseenter', function(){
                    $(this).addClass('animations-disabled');
                });

                preciseDraggable.currentProps.$stage = $('#stage');

                var $draggable = $el;
                var draggie = preciseDraggable.setDraggable({
                    $target: $draggable,
                    data: {
                        key: $draggable.attr('data-key')
                    }
                });

                draggie.on('dragStart', function(){
                    $draggable.addClass('animations-disabled');
                    preciseDraggable.currentProps.dragging = true;
                    var $this = draggie.$element;
                    preciseDraggable.currentProps.lastDraggable = {
                        data: {
                            key: $this.attr('data-key')
                        },
                        $element: $this
                    };
                    self.$slide.find('.drop').each(function () {
                        if (!$(this).data('dropped')) {
                            $(this).addClass('highlight');
                        }
                    })
                });

                draggie.on('dragMove', function(event){
                    draggie.overlap = preciseDraggable.getOverlap({
                        originalGroup: $draggable,
                        matchingGroup: $droppableElements
                    });

                    $droppableElements.trigger('draggableLeave');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableOver');
                        offset = $draggable.offset();
                        xPos = offset.left;
                        yPos = offset.top;
                        // console.log(xPos);
                        // console.log(yPos);
                    }
                })

                draggie.on('dragEnd', function(){
                    $droppableElements.removeClass('highlight over');
                    if(_.isObject(draggie.overlap)){
                        draggie.overlap.$element2.trigger('draggableDrop');
                    }
                })
            });
        }

        $("[data-slide='3'] .reiniciar").on("click", function () {
            $("[data-slide='3'] .drop").html("");
            $("[data-slide='3'] .drop").removeClass("dropeado");
            $("[data-slide='3'] .drop").data('dropped', false);

            console.log(answersImagen);
             answersImagen = [];
             console.log(answersImagen);
            module.exports.currentProps.answers[0] = answersImagen;
            console.log(module.exports.currentProps.answers);

             cargarDrags();

             $(this).addClass("oculto");
        });


        var $recurso;

        $("[data-slide='3'] .recurso").on("click", function () {
            $this = $(this);
            $recurso = $this.data("recurso");
            if($recurso != 5){
                $("[data-slide='3'] .pagina").addClass("oculto");
                $("[data-slide='3'] .pagina[data-pagina='" + ($recurso+1) + "']").removeClass("oculto");
                $("[data-slide='3'] .pagina[data-pagina='" + ($recurso+1) + "']").addClass("visto");
                $("[data-slide='3'] .acciones").addClass("oculto");
                if ($recurso != 1) 
                    $("[data-slide='3'] .acciones").removeClass("oculto");
                setTimeout(function () {
                    if($recurso == 3){
                        swal({
                            target: stageResize.currentProps.$stage.get(0),
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            customClass: 'ova-themed slide3ModalPagina',
                            showCloseButton: true,
                            showConfirmButton: false,
                            html: require("templates/slide3ModalPagina3.hbs")(),
                        });
                    }
                    if($recurso == 4){
                        swal({
                            target: stageResize.currentProps.$stage.get(0),
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            customClass: 'ova-themed slide3ModalPagina',
                            showCloseButton: true,
                            showConfirmButton: false,
                            html: require("templates/slide3ModalPagina4.hbs")(),
                        });
                    }
                }, 1000);
            }else{
                swal({
                    target: stageResize.currentProps.$stage.get(0),
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    customClass: 'ova-themed slide3P5',
                    showCloseButton: true,
                    showConfirmButton: false,
                    html: require("templates/slide3ModalPagina5.hbs")(),
                }).then(function (t) {
                    $("[data-slide='3'] .pagina[data-pagina='" + ($recurso+1) + "']").addClass("visto");  
                    setTimeout(function () {
                        $("[data-slide='3'] .recurso[data-recurso='" + $recurso + "']").removeClass("noColor");
                    }, 1000);
                    setTimeout(function () {
                        $("[data-slide='3'] .recurso[data-recurso='" + ($recurso+1) + "']").addClass("click-me");
                    }, 2000);
                    setTimeout(function () {
                        validaFinaliza();
                    }, 3000);
                });
            }
        });

        $("[data-slide='3'] .boton-enlace").on("click", function () {
            $this = $(this);
            $enlace = $this.data("boton-enlace");
            // $("[data-slide='3'] .fondo-enlace").removeClass("oculto");
            // $("[data-slide='3'] .enlace").addClass("escondido");
            // $("[data-slide='3'] .enlace[data-enlace='" + $enlace + "']").removeClass("escondido");
            $this.addClass("activo");
            if ($("[data-slide='3'] .botonera .activo").length == $("[data-slide='3'] .botonera .boton-enlace").length) {
                $("[data-slide='3'] .continuar").removeClass("oculto");
            }
        });

        $("[data-slide='3'] .continuar").on("click", function () {
            $("[data-slide='3'] .pagina").addClass("oculto");
            $("[data-slide='3'] .acciones").addClass("oculto");
            $("[data-slide='3'] .pagina[data-pagina='1']").removeClass("oculto");
            $("[data-slide='3'] .pagina[data-pagina='2']").addClass("visto");
            setTimeout(function () {
                $("[data-slide='3'] .recurso[data-recurso='1']").removeClass("noColor");
            }, 1000);
            setTimeout(function () {
                $("[data-slide='3'] .recurso[data-recurso='" + ($recurso+1) + "']").addClass("click-me");
            }, 2000);
            setTimeout(function () {
                validaFinaliza();
            }, 3000);
        });

        $("[data-slide='3'] .volver").on("click", function () {
            vuelve();
        });

        $("[data-slide='3'] .guardar").on("click", function () {
            module.exports.currentProps.answers[0] = answersImagen;
            module.exports.currentProps.answers[1] = answers[1];
            module.exports.currentProps.answers[2] = answers[2];
            console.log(module.exports.currentProps.answers);
            vuelve();
        });

        function vuelve(){
            $("[data-slide='3'] .pagina").addClass("oculto");
            $("[data-slide='3'] .acciones").addClass("oculto");
            $("[data-slide='3'] .pagina[data-pagina='1']").removeClass("oculto");
            $("[data-slide='3'] .pagina[data-pagina='" + ($recurso+1) + "']").addClass("visto");
            setTimeout(function () {
                $("[data-slide='3'] .recurso[data-recurso='" + $recurso + "']").removeClass("noColor");
            }, 1000);
            setTimeout(function () {
                $("[data-slide='3'] .recurso[data-recurso='" + ($recurso+1) + "']").addClass("click-me");
            }, 2000);
            setTimeout(function () {
                validaFinaliza();
            }, 3000);
        }

        function validaFinaliza(){
            setTimeout(function () {
                if ($("[data-slide='3'] .noColor").length == 0) {
                    $("[data-slide='3'] .finalizar").removeClass("oculto");
                }
            }, 1500);
        }

        $("[data-slide='3'] .finalizar").on("click", function () {

            console.log(module.exports.currentProps.answers);

            var pdfArgs = {};
            for (var i = 0; i < module.exports.currentProps.answers.length; i++) {
                pdfArgs[i] = {
                  answer: module.exports.currentProps.answers[i]
                };
            }

            console.log('pdfArgs', pdfArgs);

            var pdfArgs = $.param({
              pdfArgs: pdfArgs 
            });

            swal({
                target: stageResize.currentProps.$stage.get(0),
                allowOutsideClick: false,
                allowEscapeKey: false,
                customClass: 'ova-themed modalPdf',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/slide3Pdf.hbs")(  
                        {
                            downloadArguments: "?" + pdfArgs,
                            assignLink: _.get(self, 'courseActivities.assign.1.href')
                        } 
                    ),
            }).then(function (t) {
                $("[data-slide='3'] .button-continuar").removeClass("escondido");
            });
        });

        $("[data-slide='3'] .button-continuar").on("click", function () {
            presentation.switchToSlide({
              slide: $('.slide[data-slide="1"]')
            });
            setTimeout(function () {
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'actividad_2': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                });
            }, 1500);
            setTimeout(function () {
                $("[data-slide='1'] .recurso[data-recurso='3']").addClass("click-me");
            }, 2500);
        });
    }
}