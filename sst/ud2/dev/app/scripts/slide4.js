const _ = require('lodash');
const stageResize = require('stage-resize');
const swalOptions = require('swalOptions.js');
const swal = require('sweetalert2');
const presentation = require('presentation');
const courseData = require('courseData.json');

module.exports = {

	currentProps: {
	    title: ["Valoración y Evaluación de Riesgos", "Contexto de la organización", "Identificación de peligros para la seguridad y salud en el trabajo", "Análisis y evaluación del riesgo", "Gestión de los Peligros y riesgos", "Eliminación", "Sustitución", "Controles de ingeniería", "Controles administrativos", "Métodos de control respecto a los elementos de protección personal (EPP)"],
	    text: ["Se enfoca en detectar las situaciones de peligro que tengan la probabilidad de afectar la seguridad y salud en el trabajo de sus colaboradores. Se tienen en cuenta los equipos principales y auxiliares, la identificación de procesos y los antecedentes de eventos", "Reconocer la actividad económica de la empresa, la Estructura organizacional y recurso humano, los Riesgos prioritarios y del sector y los Procesos y Servicios.", "Se enfoca en detectar las situaciones de peligro que tengan la probabilidad de afectar la seguridad y salud en el trabajo de sus colaboradores. Se tienen en cuenta los equipos principales y auxiliares, la identificación de procesos y los antecedentes de eventos.", "Implica la consideración de sus consecuencias (severidad) en caso de materializarse y la medida de la posibilidad de que dicho riesgo se pueda concretar (Probabilidad). Se analizan los riesgos, los equipos y las actividades que son realizadas en cada proceso o servicio, se evalúa la calidad y suficiencia de los controles, los peligros asociados y los riesgos.", "Se determina si los controles existentes son suficientes o necesitan mejorar estableciendo nuevos controles y se definen los métodos de control necesarios para la intervención objetiva de los peligros identificados. Los métodos de control están determinados por una jerarquización, la cual está clasificada en los criterios Eliminación, Sustitución, Controles de Ingeniería, Controles administrativos y Métodos de control de los elementos de protección personal.", "Es una medida que se toma para hacer desaparecer el riesgo o el peligro.", "Es una medida que se toma a fin de reemplazar un peligro por otro. Haciendo que no se genere el riesgo o que genere menos riesgo.", "Son medidas técnicas para controlar el peligro o el riesgo desde su origen, como puede ser el confinamiento de un peligro o un proceso de trabajo.", "Medidas que tienen como fin de reducir el tiempo de exposición al peligro, como puede ser la rotación de personal, los cambios en la duración o el tipo de jornada de trabajo.", "Son medidas que se basan en la utilización de distintos dispositivos, accesorios y vestimentas por parte de los trabajadores, con el fin de protegerlos contra posibles daños a su salud o su integridad física."]
	},

    addEventListeners: function(){

    	var self = this;

        $("[data-slide='4'] .boton").on("click", function () {
        	$this = $(this);
        	$this.addClass("visto");

        	$boton = $this.data("boton");

        	swal({
        	    target: stageResize.currentProps.$stage.get(0),
        	    allowOutsideClick: false,
        	    allowEscapeKey: false,
        	    customClass: 'ova-themed slide4Modal',
        	    showCloseButton: true,
        	    showConfirmButton: false,
        	    title: module.exports.currentProps.title[$boton-1],
  				text: module.exports.currentProps.text[$boton-1],
        	}).then(function (t) {
                $this.removeClass("noColor");
        		if ($("[data-slide='4'] .visto").length == $("[data-slide='4'] .boton").length) {
        		    $("[data-slide='4'] .button-continuar").removeClass("escondido");
        		}
        	});
        });

        $("[data-slide='4'] .button-continuar").on("click", function () {
        	presentation.switchToSlide({
        	  slide: $('.slide[data-slide="1"]')
        	});
        	setTimeout(function () {
        		self.ovaProgress.updateFinishedTopics({
        		    topics: {
        		        'recurso_3': true
        		    },
        		    action: 'add',
        		    courseShortName: courseData.shortName,
        		    courseUnit: courseData.unit
        		});
        	}, 1500);
        	setTimeout(function () {
        		$("[data-slide='1'] .actividad[data-actividad='3']").addClass("click-me");
        	}, 2500);
        });
	}
}