const _ = require("lodash");
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const courseData = require('courseData.json');
const preciseDraggable = require("precise-draggable");
var $textarraytres= new Array()
const slide2 = require('slide2.js');
module.exports = {
  onEnter:function(){
    var self = this;
    console.log(self.activities)
    if (_.isObject(self.activities.AutoevaluacionPrimeraUd4.textos.a3)) {
      var rtauno = self.activities.AutoevaluacionPrimeraUd4.textos.a3;
      console.log('llego del servidor', rtauno);
      var a=-1;
      var a=-1;
      $('.cont-preguntas-act3 .cont-text textarea').each(function(){
        a++
        $(this).val(rtauno[a]);
      })
      $(".item-drop-tres").html(rtauno[3])
    }else{
      var actpartuno  = slide2.currentProps.$textarrayf.$textoareas;
      console.log("respuesta de la variable", actpartuno);
      var a=-1;
      $('.cont-preguntas-act3 .cont-text textarea').each(function(){
        a++
        $(this).val(actpartuno[a]);
      })
      $(".item-drop-tres").html(actpartuno[3])
    }
    swal({
        target: stageResize.currentProps.$stage.get(0),
        customClass: 'ova-themed modal_s7',
        showCloseButton: true,
        showConfirmButton: false,
        html: require("templates/modal_s7_1.hbs")()
     })

  },
    addEventListeners: function () {
        var self = this;

        var $draggableItems = $('.item-drag-tres');
        $('.clear_s7').on('click', function(){
          $(".item-drop-tres").html("");
          $draggableItems.css('display', 'inline-block');
        })
        preciseDraggable.currentProps.$stage = $('#stage');
        $draggableItems.each(function () {
          var draggie = preciseDraggable.setDraggable({
            $target: $(this),
            data: 1
          });
        })
        $(".item-drop-tres").on({
          'mouseover': function () {
            if (preciseDraggable.currentProps.dragging) {
              if (!$(this).data('dropped')) {
                $(this).addClass('highlight');
              }
            }
          },
          'mouseup': function () {
            if (preciseDraggable.currentProps.dragging) {
              $("#stage").removeClass('grabbing');
              preciseDraggable.currentProps.dragging = false;
              preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
              if (!$(this).data('dropped')) {
                var target = preciseDraggable.currentProps.lastDraggable.data.target;
                var $el = preciseDraggable.currentProps.lastDraggable.$element;
                $(this).data('dropped', false);
                $(this).append("<li>" + $el.html() + "</li>");
                $el.css('display', 'none');
                // $el.remove();
              }
            }
          }
        });

        $('.button-finish-s3').on("click", function(){
          $('.cont-preguntas-act3 .cont-text textarea').each(function(){
            var textoareatres = $(this).val();
            console.log(textoareatres);
            $textarraytres.push(textoareatres);
            console.log($textarraytres);
          })
          var textoitemdroptres = $(".item-drop-tres").html()
          $textarraytres.push(textoitemdroptres);
          console.log($textarraytres);
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_3.hbs")({
                  parametros: $.param({
                    r1: $textarraytres
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.1.href')
                })
              })
              .then(function (value) {
                console.log(self);
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 3': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $('.home-exp[data-exp="6" ]').addClass("disabled");
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });
            })
        })
    }
}
