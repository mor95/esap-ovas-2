const _ = require("lodash");
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const courseData = require('courseData.json');
const preciseDraggable = require("precise-draggable");
var $textarray= new Array()
var $textarrayenvio;
var $drppenvio;
module.exports = {
  currentProps: {
      $textarrayf:{
        $textoareas:0,
        $dropsact:0
      }
  },
    addEventListeners: function () {
        var self = this;
        var $draggableItems = $('.item-drag');
        preciseDraggable.currentProps.$stage = $('#stage');
        $draggableItems.each(function () {
          var draggie = preciseDraggable.setDraggable({
            $target: $(this),
            data: 1
          });
        })
        $(".item-drop").on({
          'mouseover': function () {
            if (preciseDraggable.currentProps.dragging) {
              if (!$(this).data('dropped')) {
                $(this).addClass('highlight');
              }
            }
          },
          'mouseup': function () {
            if (preciseDraggable.currentProps.dragging) {
              $("#stage").removeClass('grabbing');
              preciseDraggable.currentProps.dragging = false;
              preciseDraggable.currentProps.lastDraggable.$element.removeClass('is-dragging');
              if (!$(this).data('dropped')) {
                var target = preciseDraggable.currentProps.lastDraggable.data.target;
                var $el = preciseDraggable.currentProps.lastDraggable.$element;
                $(this).data('dropped', false);
                $(this).append("<li>" + $el.html() + "</li>");
                $el.remove();
              }
            }
          }
        });

        $('.button-finish-s2').on("click", function(){
          $('.cont-preguntas .cont-text textarea').each(function(){
            var textoarea = $(this).val();
            console.log(textoarea);
            $textarray.push(textoarea);
            console.log($textarray);
            module.exports.currentProps.$textarrayf.$textoareas = $textarray;
            $textarrayenvio = module.exports.currentProps.$textarrayf.$textoareas;
          })
          var textoitemdrop = $(".item-drop").html()
          $textarray.push(textoitemdrop);
          console.log(textoitemdrop);
          module.exports.currentProps.$textarrayf.$dropsact = textoitemdrop;
          $drppenvio = module.exports.currentProps.$textarrayf.$dropsact;
            swal({
                target: stageResize.currentProps.$stage.get(0),
                customClass: 'ova-send-activity',
                showCloseButton: true,
                showConfirmButton: false,
                html: require("templates/modal_act_1.hbs")({
                  parametros: $.param({
                    r1: $textarray
                  }),
                  assignLink: _.get(self, 'courseActivities.assign.0.href')
                })
              })
              .then(function (value) {
                console.log(self);
                self.ovaProgress.updateFinishedTopics({
                    topics: {
                        'Actividad 1': true
                    },
                    action: 'add',
                    courseShortName: courseData.shortName,
                    courseUnit: courseData.unit
                })
                $(".initial-message").css("display", "none");
                $('.home-exp[data-exp="1" ]').addClass("disabled");
                presentation.switchToSlide({
                    slide: $('.slide[data-slide="1"]')
                });

                require('init.js').APIInstance.makeRequest({//también puede ser module.exports - self en el slide
                  component: 'activities',
                  method: 'addLOActivities',
                  showFeedback: true,
                  arguments: {
                      shortName: require('courseData.json').shortName,
                      unit: require('courseData.json').unit,
                      activityId: 'AutoevaluacionPrimeraUd4',
                      answers: JSON.stringify({
                          textos: {
                            a3: $textarrayenvio,
                            b1: $drppenvio
                          }
                      })
                  }
              })
              .then(function(response){
                  console.log('response', response)
              })
            })
        })
    }
}
