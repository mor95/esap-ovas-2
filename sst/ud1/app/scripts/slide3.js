'use strict';
const swal = require("sweetalert2");
const swalOptions = require("swalOptions.js");
const stageResize = require("stage-resize");
const presentation = require('presentation');
const concepts = require("concepts.json");
const _ = require("lodash");
const courseData = require('courseData.json');
  module.exports = {
    addEventListeners: function () {
        var self = this;
        $('.btn_cont_S3').on("click", function(){
          var targetConcepts = _.pick(concepts, [
            "Normatividad",
            "Obligación",
            "Responsabilidad",
            "Sistema de Gestión de Seguridad y Salud en el Trabajo (SG-SST)",
            "Planes de intervención"
          ]);
          self.ovaConcepts.showNewConceptsModal({
              concepts: targetConcepts,
              title: '¿Cuál es la relación de estos conceptos con las estrategias de fortalecimiento en la gestión humana?',
              subtitle: 'Reto mental: Imagine una situación donde se evidencien estos conceptos como acciones. Compare sus imágenes con las definiciones que encuentra en el cofre de conceptos.'

          }).then(function (value) {
              presentation.switchToSlide({
                  slide: $('.slide[data-slide="1"]')
              });
              self.ovaConcepts.updateConcepts({
                  concepts: targetConcepts,
                  action: "insert",
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              });
              self.ovaProgress.updateFinishedTopics({
                  topics: {
                      'Recursos 1': true
                  },
                  action: 'add',
                  courseShortName: courseData.shortName,
                  courseUnit: courseData.unit
              })
          })
        })
    }
}
